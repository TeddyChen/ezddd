package tw.teddysoft.ezddd.example.eventsourced.entity;

import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.entity.EsAggregateRoot;
import tw.teddysoft.ezddd.example.standard.entity.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class EsWorkflow extends EsAggregateRoot<String, DomainEvent> {
    public final static String CATEGORY = "Workflow";
    private String boardId;
    private String workflowId;
    private String workflowName;
    private List<Stage> stages;

    @Override
    protected void ensureInvariant() {
        if (isDeleted) {
            throw new RuntimeException("Mark as deleted");
        }

        Objects.requireNonNull(boardId);
        Objects.requireNonNull(workflowId);
        Objects.requireNonNull(workflowName);
        Objects.requireNonNull(stages);
    }

    public EsWorkflow(String boardId, String workflowId, String workflowName) {
        apply(new WorkflowCreated(boardId, workflowId, workflowName, UUID.randomUUID(), Instant.now()));
    }

    public EsWorkflow(List<? extends DomainEvent> events) {
        super(events);
    }

    @Override
    public String getId() {
        return workflowId;
    }

    @Override
    protected void when(DomainEvent event) {
        if (event instanceof WorkflowCreated e){
            this.boardId = e.boardId();
            this.workflowId = e.workflowId();
            this.workflowName = e.workflowName();
            this.stages = new ArrayList<>();
        } else if (event instanceof StageCreated e) {
            stages.add(new Stage(e.stageId(), e.stageName(), e.wipLimit()));
        } else if (event instanceof WorkflowDeleted){
            this.isDeleted = true;
        }
    }

    @Override
    public String getCategory() {
        return CATEGORY;
    }

    public String getBoardId() {
        return boardId;
    }

    public String getName() {
        return workflowName;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void createStage(String stageId, String stageName, WipLimit wipLimit) {
        apply(new StageCreated(this.workflowId, stageId, stageName, wipLimit, UUID.randomUUID(), Instant.now()));
    }

    public void markAsDeleted() {
        apply(new WorkflowDeleted(this.workflowId, UUID.randomUUID(), Instant.now()));
    }
}
