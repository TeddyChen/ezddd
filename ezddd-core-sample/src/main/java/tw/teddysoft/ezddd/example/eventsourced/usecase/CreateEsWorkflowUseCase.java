package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.example.eventsourced.entity.EsWorkflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;

public class CreateEsWorkflowUseCase implements UseCase<CreateEsWorkflowInput, CreateEsWorkflowOutput> {
    private final Repository<EsWorkflow, String> repository;

    public CreateEsWorkflowUseCase(Repository<EsWorkflow, String> repository) {
        this.repository = repository;
    }

    @Override
    public CreateEsWorkflowOutput execute(CreateEsWorkflowInput input) {
        EsWorkflow workflow = new EsWorkflow(input.getBoardId(), input.getWorkflowId(), input.getWorkflowName());
        repository.save(workflow);

        CreateEsWorkflowOutput output = new CreateEsWorkflowOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);
        return output;
    }
}
