package tw.teddysoft.ezddd.example.standard.entity;

import tw.teddysoft.ezddd.core.entity.ValueObject;

public record WipLimit(int i) implements ValueObject {

    public static WipLimit valueOf(int value) { return new WipLimit(value); }
}
