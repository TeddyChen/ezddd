package tw.teddysoft.ezddd.example.standard.entity;

import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public record StageCreated(String workflowId, String stageId, String stageName, WipLimit wipLimit, UUID id, Instant occurredOn) implements DomainEvent {

    @Override
    public String aggregateId() {
        return workflowId;
    }
}
