package tw.teddysoft.ezddd.example.standard.entity;

import tw.teddysoft.ezddd.core.entity.DomainEventTypeMapper;

public class WorkflowEventTypeMapper extends DomainEventTypeMapper.DefaultMapper {

    public static final String MAPPING_TYPE_PREFIX = "WorkflowEvents$";
    public static final String WORKFLOW_CREATED = MAPPING_TYPE_PREFIX + "WorkflowCreated";
    public static final String STAGE_CREATED = MAPPING_TYPE_PREFIX + "StageCreated";
    public static final String WORKFLOW_DELETED = MAPPING_TYPE_PREFIX + "WorkflowDeleted";

    private static final DomainEventTypeMapper mapper;

    static {
        mapper = DomainEventTypeMapper.create();
        mapper.put(WORKFLOW_CREATED, WorkflowCreated.class);
        mapper.put(STAGE_CREATED, StageCreated.class);
        mapper.put(WORKFLOW_DELETED, WorkflowDeleted.class);
    }

    public static DomainEventTypeMapper getInstance(){
        return mapper;
    }


}
