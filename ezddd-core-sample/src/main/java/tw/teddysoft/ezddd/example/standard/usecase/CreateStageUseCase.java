package tw.teddysoft.ezddd.example.standard.usecase;

import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;

public class CreateStageUseCase implements UseCase<CreateStageInput, CreateStageOutput> {
    private final Repository<Workflow, String> workflowRepository;

    public CreateStageUseCase(Repository<Workflow, String> workflowRepository) {
        this.workflowRepository = workflowRepository;
    }

    public CreateStageOutput execute(CreateStageInput input) {
        Workflow workflow = workflowRepository.findById(input.getWorkflowId()).get();
        workflow.createStage(input.getStageId(), input.getStageName(), input.getWipLimit());
        workflowRepository.save(workflow);
        CreateStageOutput output = new CreateStageOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);
        return output;
    }
}
