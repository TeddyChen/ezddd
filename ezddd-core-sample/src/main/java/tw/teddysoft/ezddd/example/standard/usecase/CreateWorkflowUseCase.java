package tw.teddysoft.ezddd.example.standard.usecase;

import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;

public class CreateWorkflowUseCase implements UseCase<CreateWorkflowInput, CreateWorkflowOutput> {
    private final Repository<Workflow, String> repository;

    public CreateWorkflowUseCase(Repository<Workflow, String> repository) {
        this.repository = repository;
    }

    public CreateWorkflowOutput execute(CreateWorkflowInput input) {
        Workflow workflow = new Workflow(input.getBoardId(), input.getWorkflowId(), input.getWorkflowName());
        repository.save(workflow);

        CreateWorkflowOutput output = new CreateWorkflowOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);
        return output;
    }
}
