package tw.teddysoft.ezddd.example.standard.usecase;

import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Output;

public class DeleteWorkflowOutput implements Output {
    private String id;
    private ExitCode exitCode;
    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Output setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public ExitCode getExitCode() {
        return exitCode;
    }

    @Override
    public Output setExitCode(ExitCode exitCode) {
        this.exitCode = exitCode;
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Output setId(String id) {
        this.id = id;
        return this;
    }
}
