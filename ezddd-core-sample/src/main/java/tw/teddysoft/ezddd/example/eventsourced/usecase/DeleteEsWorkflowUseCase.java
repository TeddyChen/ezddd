package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.example.eventsourced.entity.EsWorkflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;

public class DeleteEsWorkflowUseCase implements UseCase<DeleteEsWorkflowInput, DeleteEsWorkflowOutput> {

    private final Repository<EsWorkflow, String> repository;

    public DeleteEsWorkflowUseCase(Repository<EsWorkflow, String> repository) {
        this.repository = repository;
    }

    @Override
    public DeleteEsWorkflowOutput execute(DeleteEsWorkflowInput input) {
        EsWorkflow workflow = repository.findById(input.getWorkflowId()).get();

        workflow.markAsDeleted();
        repository.delete(workflow);

        DeleteEsWorkflowOutput output = new DeleteEsWorkflowOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);

        return output;

    }
}
