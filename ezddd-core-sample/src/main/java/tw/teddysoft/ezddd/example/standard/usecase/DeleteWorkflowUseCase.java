package tw.teddysoft.ezddd.example.standard.usecase;

import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;

public class DeleteWorkflowUseCase implements UseCase<DeleteWorkflowInput, DeleteWorkflowOutput> {
    private final Repository<Workflow, String> repository;

    public DeleteWorkflowUseCase(Repository<Workflow, String> repository) {
        this.repository = repository;
    }

    public DeleteWorkflowOutput execute(DeleteWorkflowInput input) {
        Workflow workflow = repository.findById(input.getWorkflowId()).get();

        workflow.markAsDeleted();
        repository.delete(workflow);

        DeleteWorkflowOutput output = new DeleteWorkflowOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);

        return output;
    }

}
