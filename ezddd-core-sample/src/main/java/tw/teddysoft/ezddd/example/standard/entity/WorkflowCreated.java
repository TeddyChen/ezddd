package tw.teddysoft.ezddd.example.standard.entity;


import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public record WorkflowCreated(String boardId, String workflowId, String workflowName, UUID id, Instant occurredOn) implements DomainEvent.ConstructionEvent {

    @Override
    public String aggregateId() {
        return workflowId;
    }
}
