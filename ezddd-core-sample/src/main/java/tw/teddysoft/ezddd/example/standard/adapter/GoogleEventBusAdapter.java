package tw.teddysoft.ezddd.example.standard.adapter;

import com.google.common.eventbus.EventBus;
import tw.teddysoft.ezddd.core.entity.AggregateRoot;
import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;

import java.util.ArrayList;
import java.util.List;

public class GoogleEventBusAdapter extends EventBus implements DomainEventBus {

    public GoogleEventBusAdapter() {
        super();
    }

    @Override
    public void post(DomainEvent domainEvent){
        super.post(domainEvent);
    }

    @Override
    public void postAll(AggregateRoot aggregateRoot) {
        List<DomainEvent> domainEvents = new ArrayList<>(aggregateRoot.getDomainEvents());
        aggregateRoot.clearDomainEvents();

        domainEvents.stream().forEach( x-> super.post(x));
        domainEvents.clear();
    }

}
