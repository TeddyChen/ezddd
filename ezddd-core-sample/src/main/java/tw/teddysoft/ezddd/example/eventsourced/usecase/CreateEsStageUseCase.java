package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.example.eventsourced.entity.EsWorkflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;

public class CreateEsStageUseCase implements UseCase<CreateEsStageInput, CreateEsStageOutput> {

    private final Repository<EsWorkflow, String> repository;

    public CreateEsStageUseCase(Repository<EsWorkflow, String> repository) {
        this.repository = repository;
    }

    @Override
    public CreateEsStageOutput execute(CreateEsStageInput input){
        EsWorkflow workflow = repository.findById(input.getWorkflowId()).get();
        workflow.createStage(input.getStageId(), input.getStageName(), input.getWipLimit());
        repository.save(workflow);
        CreateEsStageOutput output = new CreateEsStageOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);
        return output;
    }
}
