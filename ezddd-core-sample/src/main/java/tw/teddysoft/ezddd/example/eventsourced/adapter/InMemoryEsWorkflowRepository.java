package tw.teddysoft.ezddd.example.eventsourced.adapter;

import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.example.eventsourced.entity.EsWorkflow;
import tw.teddysoft.ezddd.example.standard.entity.WorkflowDeleted;

import java.util.*;

public class InMemoryEsWorkflowRepository implements Repository<EsWorkflow, String> {
    private final DomainEventBus domainEventBus;
    private final Map<String, List<DomainEvent>> eventStore;

    public InMemoryEsWorkflowRepository(DomainEventBus domainEventBus) {
        this.domainEventBus = domainEventBus;
        eventStore = new HashMap<>();
    }

    @Override
    public Optional<EsWorkflow> findById(String id) {
        List<DomainEvent> domainEvents = eventStore.get(buildStreamName(id));
        if (null == domainEvents) {
            return Optional.empty();
        }
        if (domainEvents.get(domainEvents.size() - 1) instanceof WorkflowDeleted){
            return Optional.empty();
        }
        return Optional.of(new EsWorkflow(domainEvents));
    }

    @Override
    public void save(EsWorkflow data) {
        if (data.getDomainEvents().isEmpty()) {
            return;
        }
        if (eventStore.containsKey(buildStreamName(data.getId()))) {
            eventStore.get(buildStreamName(data.getId())).addAll(data.getDomainEvents());
        } else {
            eventStore.put(buildStreamName(data.getId()), new ArrayList<>(data.getDomainEvents()));
        }
        domainEventBus.postAll(data);
    }

    @Override
    public void delete(EsWorkflow data) {
        this.save(data);
    }

    private String buildStreamName(String id) {
        StringBuilder sb = new StringBuilder();
        sb.append(EsWorkflow.CATEGORY)
                .append("-")
                .append(id);
        return sb.toString();
    }
}
