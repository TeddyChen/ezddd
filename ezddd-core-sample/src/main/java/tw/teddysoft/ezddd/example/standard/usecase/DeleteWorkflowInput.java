package tw.teddysoft.ezddd.example.standard.usecase;

import tw.teddysoft.ezddd.core.usecase.Input;

public class DeleteWorkflowInput implements Input {
    private String workflowId;
    private String name;

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
