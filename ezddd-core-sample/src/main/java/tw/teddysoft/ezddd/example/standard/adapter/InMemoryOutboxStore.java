package tw.teddysoft.ezddd.example.standard.adapter;

import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventData;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryOutboxStore {
    private final Map<String, Object> states;
    private final Map<String, List<DomainEventData>> domainEventDatas;

    public InMemoryOutboxStore() {
        states = new HashMap<>();
        domainEventDatas = new HashMap<>();
    }

    public void saveState(String id, Object data){
        states.put(id, data);
    }

    public void saveDomainEvent(String streamName, List<DomainEventData> domainEventData){
        domainEventDatas.put(streamName, domainEventData);
    }

    public void delete(String id){
        states.remove(id);
    }

    public Optional<Object> findById(String id){
        return Optional.ofNullable(states.get(id));
    }

    public List<DomainEvent> getDomainEvents(){
        List<DomainEvent> domainEvents = domainEventDatas.values()
                .stream()
                .flatMap(x -> x.stream())
                .map(x -> (DomainEvent)DomainEventMapper.toDomain(x))
                .collect(Collectors.toList());
        domainEvents.sort(Comparator.comparing(DomainEvent::occurredOn));

        return domainEvents;
    }
}
