package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Output;

public class CreateEsWorkflowOutput implements Output {
    private String id;
    private ExitCode exitCode;
    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public CreateEsWorkflowOutput setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public ExitCode getExitCode() {
        return exitCode;
    }

    @Override
    public CreateEsWorkflowOutput setExitCode(ExitCode exitCode) {
        this.exitCode = exitCode;
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public CreateEsWorkflowOutput setId(String workflowId) {
        id = workflowId;
        return this;
    }
}
