package tw.teddysoft.ezddd.example.standard.usecase;

import tw.teddysoft.ezddd.example.standard.entity.Stage;
import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;

public class WorkflowOutboxMapper implements OutboxMapper<Workflow, WorkflowData> {

    @Override
    public Workflow toDomain(WorkflowData data) {
        Workflow workflow = new Workflow(data.getBoardId(), data.getId(), data.getName());

        for (Stage stage: data.getStages()){
            workflow.createStage(stage.getId(), stage.getStageName(), stage.getWipLimit());
        }

        workflow.clearDomainEvents();
        return workflow;
    }

    @Override
    public WorkflowData toData(Workflow aggregateRoot) {
        WorkflowData workflowData = new WorkflowData();

        workflowData.setId(aggregateRoot.getId());
        workflowData.setName(aggregateRoot.getName());
        workflowData.setVersion(aggregateRoot.getVersion());
        workflowData.setBoardId(aggregateRoot.getBoardId());
        workflowData.setStages(aggregateRoot.getStages());
        workflowData.setDomainEventDatas(DomainEventMapper.toData(aggregateRoot.getDomainEvents()));

        return workflowData;
    }
}
