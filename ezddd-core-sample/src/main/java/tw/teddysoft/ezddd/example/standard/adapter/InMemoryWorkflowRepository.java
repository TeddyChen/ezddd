package tw.teddysoft.ezddd.example.standard.adapter;

import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;

import java.util.*;

public class InMemoryWorkflowRepository implements Repository<Workflow, String> {
    private final DomainEventBus domainEventBus;
    private final Map<String, Workflow> workflows;

    public InMemoryWorkflowRepository(DomainEventBus domainEventBus) {
        this.domainEventBus = domainEventBus;
        workflows = new HashMap<>();
    }

    @Override
    public Optional<Workflow> findById(String id) {
        return Optional.ofNullable(workflows.get(id));
    }

    @Override
    public void save(Workflow workflow) {
        workflows.put(workflow.getId(), workflow);
        domainEventBus.postAll(workflow);
    }

    @Override
    public void delete(Workflow workflow) {
        workflows.remove(workflow.getId());
        domainEventBus.postAll(workflow);
    }
}
