package tw.teddysoft.ezddd.example.standard.adapter;

import tw.teddysoft.ezddd.example.standard.entity.Stage;
import tw.teddysoft.ezddd.example.standard.usecase.WorkflowData;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;

import java.util.*;

public class InMemoryWorkflowRepositoryPeer implements RepositoryPeer<WorkflowData, String> {

    private final InMemoryOutboxStore outboxStore;

    public InMemoryWorkflowRepositoryPeer(InMemoryOutboxStore outboxStore) {
        this.outboxStore = outboxStore;
    }

    @Override
    public Optional<WorkflowData> findById(String workflowId) {

        if(outboxStore.findById(workflowId).isEmpty()){
            return Optional.empty();
        }
        WorkflowState state = (WorkflowState)outboxStore.findById(workflowId).get();
        WorkflowData workflowData = new WorkflowData();

        workflowData.setId(state.getWorkflowId());
        workflowData.setName(state.getName());
        workflowData.setVersion(state.getVersion());
        workflowData.setBoardId(state.getBoardId());
        workflowData.setStages(new ArrayList<>(state.getStages()));

        return Optional.of(workflowData);
    }

    @Override
    public void save(WorkflowData data) {
        outboxStore.saveState(data.getId(), toState(data));
        outboxStore.saveDomainEvent(data.getStreamName(), data.getDomainEventDatas());
    }

    @Override
    public void delete(WorkflowData data) {
        outboxStore.delete(data.getId());
        outboxStore.saveDomainEvent(data.getStreamName(), data.getDomainEventDatas());
    }

    private WorkflowState toState(WorkflowData workflowData) {

        WorkflowState workflowState = new WorkflowState();

        workflowState.setName(workflowData.getName());
        workflowState.setStages(workflowData.getStages());
        workflowState.setVersion(workflowData.getVersion());
        workflowState.setBoardId(workflowData.getBoardId());
        workflowState.setWorkflowId(workflowData.getId());

        return workflowState;
    }

    private class WorkflowState {

        private long version;
        private String boardId;
        private String workflowId;
        private String name;
        private List<Stage> stages;

        public WorkflowState() {
            stages = new ArrayList<>();
        }

        public long getVersion() {
            return version;
        }

        public void setVersion(long version) {
            this.version = version;
        }

        public String getBoardId() {
            return boardId;
        }

        public void setBoardId(String boardId) {
            this.boardId = boardId;
        }

        public String getWorkflowId() {
            return workflowId;
        }

        public void setWorkflowId(String workflowId) {
            this.workflowId = workflowId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Stage> getStages() {
            return stages;
        }

        public void setStages(List<Stage> stages) {
            this.stages = stages;
        }
    }
}
