package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.example.standard.entity.WipLimit;
import tw.teddysoft.ezddd.core.usecase.Input;

public class CreateEsStageInput implements Input {

    private String workflowId;
    private String stageId;
    private String stageName;
    private WipLimit wipLimit;

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public WipLimit getWipLimit() {
        return wipLimit;
    }

    public void setWipLimit(WipLimit wipLimit) {
        this.wipLimit = wipLimit;
    }
}
