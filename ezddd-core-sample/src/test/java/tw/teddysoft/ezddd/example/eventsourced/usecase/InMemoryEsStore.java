package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventData;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.es.EventStoreData;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryEsStore {
    private final Map<String, List<DomainEventData>> eventDatas;

    public InMemoryEsStore() {
        eventDatas = new HashMap<>();
    }

    public void save(EventStoreData data) {
        if (eventDatas.containsKey(data.getStreamName())) {
            eventDatas.get(data.getStreamName()).addAll(data.getDomainEventDatas());
        } else {
            List<DomainEventData> list = new ArrayList<>();
            list.addAll(data.getDomainEventDatas());
            eventDatas.put(data.getStreamName(), list);
        }
    }

    public List<DomainEventData> findByStreamName(String streamName) {
        if (eventDatas.containsKey(streamName)) {
            return eventDatas.get(streamName);
        }
        return new ArrayList<>();
    }

    public List<DomainEvent> getDomainEvents(){
        List<DomainEvent> domainEvents = eventDatas.values()
                .stream()
                .flatMap(x -> x.stream())
                .map(x -> (DomainEvent) DomainEventMapper.toDomain(x))
                .collect(Collectors.toList());
        domainEvents.sort(Comparator.comparing(DomainEvent::occurredOn));

        return domainEvents;
    }
}
