package tw.teddysoft.ezddd.example.eventsourced.usecase;

import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventData;
import tw.teddysoft.ezddd.core.usecase.es.EventStoreData;

import java.util.List;
import java.util.Optional;

public class InMemoryEsWorkflowRepositoryPeer implements RepositoryPeer<EventStoreData, String> {

    private final InMemoryEsStore esStore;

    public InMemoryEsWorkflowRepositoryPeer(InMemoryEsStore esStore) {
        this.esStore = esStore;
    }

    @Override
    public Optional<EventStoreData> findById(String id) {
        List<DomainEventData> workflowData = esStore.findByStreamName(id);
        if (workflowData.isEmpty() || workflowData.get(workflowData.size()-1).eventType().equals("WorkflowEvents$WorkflowDeleted")) {
            return Optional.empty();
        }

        EventStoreData eventStoreData = new EventStoreData();
        eventStoreData.setDomainEventDatas(workflowData);
        eventStoreData.setVersion(workflowData.size() - 1);
        eventStoreData.setStreamName(id);
        return Optional.of(eventStoreData);
    }

    @Override
    public void save(EventStoreData data) {
        esStore.save(data);
    }

    @Override
    public void delete(EventStoreData data) {
        this.save(data);
    }
}
