package tw.teddysoft.ezddd.example.eventsourced.usecase;

import org.json.JSONException;
import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;

import java.util.concurrent.TimeUnit;

public class EsStoreListener implements Runnable {
    private int pollingInterval;
    private final DomainEventBus domainEventBus;
    private final InMemoryEsStore esStore;
    private boolean keepRunning;
    private int currentIndex;

    public EsStoreListener(int pollingInterval, DomainEventBus domainEventBus, InMemoryEsStore esStore) {
        this.pollingInterval = pollingInterval;
        this.domainEventBus = domainEventBus;
        this.esStore = esStore;
        keepRunning = true;
        currentIndex = -1;
    }

    @Override
    public void run() {
        while (keepRunning) {
            try {
                if (currentIndex < esStore.getDomainEvents().size()-1) {
                    for (DomainEvent domainEvent: esStore.getDomainEvents().subList(currentIndex+1, esStore.getDomainEvents().size())) {
                        domainEventBus.post(domainEvent);
                        currentIndex++;
                    }
                }

                TimeUnit.MILLISECONDS.sleep(pollingInterval);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException ie) {
                keepRunning = false;
                Thread.currentThread().interrupt();
            }
        }
    }

    public void shutdown(){
        keepRunning = false;
        Thread.currentThread().interrupt();
    }
}
