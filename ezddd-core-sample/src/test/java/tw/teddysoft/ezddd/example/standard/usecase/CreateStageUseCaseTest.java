package tw.teddysoft.ezddd.example.standard.usecase;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.example.standard.adapter.GoogleEventBusAdapter;
import tw.teddysoft.ezddd.example.standard.adapter.InMemoryWorkflowRepository;
import tw.teddysoft.ezddd.example.standard.entity.StageCreated;
import tw.teddysoft.ezddd.example.standard.entity.WipLimit;
import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateStageUseCaseTest {
    @Test
    public void create_a_stage() {
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        Repository<Workflow, String> workflowRepository = new InMemoryWorkflowRepository(domainEventBus);
        UseCase<CreateWorkflowInput, CreateWorkflowOutput> createWorkflowUseCase = new CreateWorkflowUseCase(workflowRepository);
        CreateWorkflowInput createWorkflowInput = new CreateWorkflowInput();
        createWorkflowInput.setBoardId("board id");
        createWorkflowInput.setWorkflowId(UUID.randomUUID().toString());
        createWorkflowInput.setWorkflowName("workflow name");
        CreateWorkflowOutput createWorkflowOutput = createWorkflowUseCase.execute(createWorkflowInput);

        UseCase<CreateStageInput, CreateStageOutput> createStageUseCase = new CreateStageUseCase(workflowRepository);
        CreateStageInput input = new CreateStageInput();
        input.setWorkflowId(createWorkflowOutput.getId());
        input.setStageId(UUID.randomUUID().toString());
        input.setStageName("stage name");
        input.setWipLimit(WipLimit.valueOf(3));
        CreateStageOutput output = createStageUseCase.execute(input);

        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        Workflow workflow = workflowRepository.findById(input.getWorkflowId()).get();
        assertEquals(1, workflow.getStages().size());
        assertEquals(input.getStageId(), workflow.getStages().get(0).getId());
        assertEquals(input.getStageName(), workflow.getStages().get(0).getStageName());
        assertEquals(input.getWipLimit(), workflow.getStages().get(0).getWipLimit());
        assertEquals(1, fakeListener.notifyCount);
    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenStageCreated(StageCreated event) {
            notifyCount++;
        }
    }
}
