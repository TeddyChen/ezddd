package tw.teddysoft.ezddd.example.standard.usecase;

import com.google.common.eventbus.Subscribe;
import tw.teddysoft.ezddd.example.standard.adapter.*;
import tw.teddysoft.ezddd.example.standard.entity.Workflow;
import tw.teddysoft.ezddd.example.standard.entity.WorkflowCreated;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.example.standard.entity.WorkflowEventTypeMapper;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxRepository;

import java.util.UUID;
import java.util.concurrent.Executors;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateWorkflowUseCaseTest {
    @Test
    public void create_workflow() {
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        Repository<Workflow, String> repository = new InMemoryWorkflowRepository(domainEventBus);
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);

        UseCase<CreateWorkflowInput, CreateWorkflowOutput> createWorkflowUseCase = new CreateWorkflowUseCase(repository);
        CreateWorkflowInput input = new CreateWorkflowInput();
        input.setBoardId("boardId");
        input.setWorkflowId(UUID.randomUUID().toString());
        input.setWorkflowName("name");
        CreateWorkflowOutput output = createWorkflowUseCase.execute(input);

        assertEquals(input.getWorkflowId(), output.getId());
        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertTrue(repository.findById(output.getId()).isPresent());
        Workflow workflow = repository.findById(output.getId()).get();
        assertEquals("boardId", workflow.getBoardId());
        assertEquals(input.getWorkflowId(), workflow.getId());
        assertEquals("name", workflow.getName());
        assertEquals(1, fakeListener.notifyCount);
    }

    @Test
    public void create_workflow_with_outbox_repository() {

        DomainEventMapper.setMapper(WorkflowEventTypeMapper.getInstance());
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        InMemoryOutboxStore outboxStore = new InMemoryOutboxStore();
        OutboxStoreListener outboxStoreListener = new OutboxStoreListener(500, domainEventBus, outboxStore);
        RepositoryPeer<WorkflowData, String> peer = new InMemoryWorkflowRepositoryPeer(outboxStore);
        OutboxMapper<Workflow, WorkflowData> mapper = new WorkflowOutboxMapper();
        Repository<Workflow, String> repository = new OutboxRepository<Workflow, WorkflowData, String>(peer, mapper);
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        var executor = Executors.newFixedThreadPool(5);
        executor.execute(outboxStoreListener);

        UseCase<CreateWorkflowInput, CreateWorkflowOutput> createWorkflowUseCase = new CreateWorkflowUseCase(repository);
        CreateWorkflowInput input = new CreateWorkflowInput();
        input.setBoardId("boardId");
        input.setWorkflowId(UUID.randomUUID().toString());
        input.setWorkflowName("name");
        CreateWorkflowOutput output = createWorkflowUseCase.execute(input);

        assertEquals(input.getWorkflowId(), output.getId());
        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertTrue(repository.findById(output.getId()).isPresent());
        Workflow workflow = repository.findById(output.getId()).get();
        assertEquals("boardId", workflow.getBoardId());
        assertEquals(input.getWorkflowId(), workflow.getId());
        assertEquals("name", workflow.getName());

        await().untilAsserted(() -> assertEquals(1, fakeListener.notifyCount));
    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenWorkflowCreated(WorkflowCreated event) {
            notifyCount++;
        }
    }
}
