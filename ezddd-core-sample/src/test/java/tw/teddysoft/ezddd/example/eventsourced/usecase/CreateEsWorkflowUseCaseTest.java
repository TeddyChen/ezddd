package tw.teddysoft.ezddd.example.eventsourced.usecase;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.example.eventsourced.entity.EsWorkflow;
import tw.teddysoft.ezddd.example.eventsourced.adapter.InMemoryEsWorkflowRepository;
import tw.teddysoft.ezddd.example.standard.adapter.GoogleEventBusAdapter;
import tw.teddysoft.ezddd.example.standard.entity.WorkflowCreated;
import tw.teddysoft.ezddd.example.standard.entity.WorkflowEventTypeMapper;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.es.EsRepository;
import tw.teddysoft.ezddd.core.usecase.es.EventStoreData;

import java.util.UUID;
import java.util.concurrent.Executors;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateEsWorkflowUseCaseTest {
    @Test
    public void create_workflow() {
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        Repository<EsWorkflow, String> repository = new InMemoryEsWorkflowRepository(domainEventBus);
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);

        UseCase<CreateEsWorkflowInput, CreateEsWorkflowOutput> createEsWorkflowUseCase = new CreateEsWorkflowUseCase(repository);
        CreateEsWorkflowInput input = new CreateEsWorkflowInput();
        input.setBoardId("boardId");
        input.setWorkflowId(UUID.randomUUID().toString());
        input.setWorkflowName("name");
        CreateEsWorkflowOutput output = createEsWorkflowUseCase.execute(input);

        assertEquals(input.getWorkflowId(), output.getId());
        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertTrue(repository.findById(output.getId()).isPresent());
        EsWorkflow workflow = repository.findById(output.getId()).get();
        assertEquals("boardId", workflow.getBoardId());
        assertEquals(input.getWorkflowId(), workflow.getId());
        assertEquals("name", workflow.getName());
        assertEquals(1, fakeListener.notifyCount);
    }

    @Test
    public void create_workflow_with_es_repository() {
        DomainEventMapper.setMapper(WorkflowEventTypeMapper.getInstance());
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        InMemoryEsStore esStore = new InMemoryEsStore();
        EsStoreListener esStoreListener = new EsStoreListener(500, domainEventBus, esStore);
        RepositoryPeer<EventStoreData, String> peer = new InMemoryEsWorkflowRepositoryPeer(esStore);
        Repository<EsWorkflow, String> repository = new EsRepository<EsWorkflow, String>(peer, EsWorkflow.class, EsWorkflow.CATEGORY);
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        var executor = Executors.newFixedThreadPool(5);
        executor.execute(esStoreListener);

        UseCase<CreateEsWorkflowInput, CreateEsWorkflowOutput> createEsWorkflowUseCase = new CreateEsWorkflowUseCase(repository);
        CreateEsWorkflowInput input = new CreateEsWorkflowInput();
        input.setBoardId("boardId");
        input.setWorkflowId(UUID.randomUUID().toString());
        input.setWorkflowName("name");
        CreateEsWorkflowOutput output = createEsWorkflowUseCase.execute(input);

        assertEquals(input.getWorkflowId(), output.getId());
        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertTrue(repository.findById(output.getId()).isPresent());
        EsWorkflow workflow = repository.findById(output.getId()).get();
        assertEquals("boardId", workflow.getBoardId());
        assertEquals(input.getWorkflowId(), workflow.getId());
        assertEquals("name", workflow.getName());

        await().untilAsserted(() -> assertEquals(1, fakeListener.notifyCount));
    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenWorkflowCreated(WorkflowCreated event) {
            notifyCount++;
        }
    }
}
