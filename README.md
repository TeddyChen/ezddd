# ezddd

ezddd is a Java library for implementing tactical design patterns of Domain-Driven Design (DDD), Command Query Responsibility Segregation (CQRS) and Clean Architecture (CA). It contains two maven projects:

- [ezddd-core](./ezddd-core)
- [ezCQRS](./ezcqrs)
