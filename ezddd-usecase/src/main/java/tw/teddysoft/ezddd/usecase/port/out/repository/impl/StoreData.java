package tw.teddysoft.ezddd.usecase.port.out.repository.impl;

import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;

import java.util.List;

/**
 * {@code StoreData} is the persistent data of aggregate.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface StoreData<ID> {

    long getVersion();

    void setVersion(long version);

    ID getId();

    void setId(ID id);

    List<DomainEventData> getDomainEventDatas();

    void setDomainEventDatas(List<DomainEventData> domainEventDatas);

    String getStreamName();

    void setStreamName(String streamName);

    default long getOptimisticLockVersion() {
        return  getVersion() - getDomainEventDatas().size();
    }
}
