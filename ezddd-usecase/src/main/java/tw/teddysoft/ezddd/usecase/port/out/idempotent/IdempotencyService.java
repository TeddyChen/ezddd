package tw.teddysoft.ezddd.usecase.port.out.idempotent;

import java.util.Optional;

public interface IdempotencyService {
    Optional<EventProcessingStatus> getEventHandlingStatus(String eventId);

    boolean isEventHandled(String eventId);

    default boolean isEventActive(String eventId) {
        return getEventHandlingStatus(eventId)
                .map(status -> status.equals(EventProcessingStatus.ACTIVE))
                .orElse(false);
    }

    default boolean isEventCommitted(String eventId) {
        return getEventHandlingStatus(eventId)
                .map(status -> status.equals(EventProcessingStatus.COMMITTED))
                .orElse(false);
    }

    void activateEventHandling(String eventId);

    void commitEventHandling(String eventId);
}
