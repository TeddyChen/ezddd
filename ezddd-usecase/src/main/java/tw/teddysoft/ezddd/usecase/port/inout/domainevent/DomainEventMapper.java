package tw.teddysoft.ezddd.usecase.port.inout.domainevent;

import tw.teddysoft.ezddd.common.Json;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

/**
 * {@code DomainEventMapper} is a class to
 * (1) map a {@code DomainEventData} to {@code DomainEvent}.
 * (2) map a {@code DomainEvent} to {@code DomainEventData}.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class DomainEventMapper {
    public static final String RAW_TYPE = "rawType";

    private static DomainEventTypeMapper mapper = null;

    public static DomainEventTypeMapper getMapper() {
        return mapper;
    }

    public static void setMapper(DomainEventTypeMapper newMapper) {
        mapper = newMapper;
    }

    public static DomainEventData toData(DomainEvent event) {
        requireNotNull("DomainEvent", event);

        return DomainEventDataBuilderJava8.json(
                        mapper.toMappingType(event.getClass()),
                        event)
                .eventId(event.id())
                .build();
    }

    public static List<DomainEventData> toData(List<DomainEvent> events) {
        requireNotNull("DomainEvent", events);

        return events.stream().map(DomainEventMapper::toData).collect(Collectors.toList());
    }

    public static <T extends DomainEvent> T toDomain(DomainEventData data) {
        requireNotNull("DomainEventData", data);
        requireNotNull("Please call setMapper to config public class DomainEventMapper first", mapper);

        T domainEvent = null;
        try {
            domainEvent = (T) Json.readAs(data.eventBody(), mapper.toClass(data.eventType()));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return domainEvent;
    }

    public static <T extends DomainEvent> List<T> toDomain(List<DomainEventData> datas) {
        requireNotNull("DomainEventData list", datas);

        List<T> result = new ArrayList<>();
        datas.forEach( x -> result.add(toDomain(x)));
        return result;
    }
//     TODO: unused. discuss to remove
//    private static String getEventRawTypeMetadata(DomainEvent event) {
//        DomainEventMetadata metadata = new DomainEventMetadata();
//        metadata.append(RAW_TYPE, event.getClass().getName());
//
//        return metadata.asJsonString();
//    }

}
