package tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox;

import tw.teddysoft.ezddd.entity.AggregateRoot;

/**
 * {@code OutboxMapper} is an interface to map a {@code AggregateRoot} to
 * {@code OutboxData} which stored in state sourcing database.
 *
 * @param <T> the type parameter for representing an aggregate root
 * @param <E> the type parameter for representing an aggregate root data stored
 *           in state sourcing database
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface OutboxMapper<T extends AggregateRoot, E extends OutboxData> {

    T toDomain(E data);

    E toData(T aggregateRoot);
}
