package tw.teddysoft.ezddd.usecase.port.inout.messaging;

import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;

/**
 * {@link MessageBus} is an interface for broadcasting messages to subscribers
 * (i.e., {@link Reactor})inside a bounded context.
 * You can publish messages via the {@link #post(Object)} method.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 2.0
 */

public interface MessageBus<Message> {
    void register(Reactor<Message> reactor);
    void unregister(Reactor<Message> reactor);
    void post(Message message);
}
