package tw.teddysoft.ezddd.usecase.port.out.repository.impl;

/**
 * {@code RepositoryPeerSaveException} is the subclass of {@code RuntimeException}.
 * When a {@code RepositoryPeer} fail to save (@code StoreData},
 * {@code RepositoryPeerSaveException} will be thrown.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class RepositoryPeerSaveException extends RuntimeException {
    public static final String OPTIMISTIC_LOCKING_FAILURE = "Optimistic locking failure";

    public RepositoryPeerSaveException() {
        super();
    }

    public RepositoryPeerSaveException(Exception e) {
        super(e);
    }

    public RepositoryPeerSaveException(String message) {
        super(message);
    }

    public RepositoryPeerSaveException(String message, Throwable cause) {
        super(message, cause);
    }

}
