package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code Reactor} takes care of specific business rules whenever it receives
 * a message from the {@code MessageBus}. According to the received message,
 * a {@code Reactor} triggers a side effect such as updating an aggregate,
 * notifying frontend clients, or notifying another bounded context.
 * In addition, {@code Reactor} ensures idempotent handling of messages.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 2.0
 */

public interface Reactor<Input> {
    void execute(Input input);
}