package tw.teddysoft.ezddd.usecase.port.out.repository;

/**
 * {@code RepositorySaveException} is the subclass of {@code RuntimeException}.
 * When a {@code Repository} fail to save aggregate root data,
 * {@code RepositorySaveException} will be thrown.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class RepositorySaveException extends RuntimeException{
    public static final String OPTIMISTIC_LOCKING_FAILURE = "Optimistic locking failure";

    public RepositorySaveException(){
        super();
    }

    public RepositorySaveException(Exception e){
        super(e);
    }

    public RepositorySaveException(String message){
        super(message);
    }

    public RepositorySaveException(String message, Throwable cause) {
        super(message, cause);
    }

}
