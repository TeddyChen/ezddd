package tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox;

import tw.teddysoft.ezddd.entity.AggregateRoot;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeerSaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.RepositorySaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.Repository;

import java.util.Optional;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

/**
 * {@code OutboxRepository} is a generic implementation of {@code Repository}
 * for state sourcing.
 *
 * @param <T> the type parameter for representing aggregate root data using
 *           event sourcing
 * @param <E> the type parameter for representing aggregate root data using
 *           state sourcing
 * @param <ID> the type parameter for representing aggregate id
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class OutboxRepository<T extends AggregateRoot, E extends OutboxData, ID> implements Repository<T, ID> {

    private RepositoryPeer<E, String> repositoryPeer;
    private final OutboxMapper<T, E> mapper;

    public OutboxRepository(RepositoryPeer<E, String> repositoryPeer, OutboxMapper mapper) {
        this.repositoryPeer = repositoryPeer;
        this.mapper = mapper;
    }

    @Override
    public Optional<T> findById(ID id) {
        requireNotNull("id", id);
        Optional<E> data = repositoryPeer.findById(id.toString());
        if (data.isPresent()) {
            return Optional.of(mapper.toDomain(data.get()));
        }
        return Optional.empty();
    }

    @Override
    public void save(T aggregate) {
        try {
            E data = mapper.toData(aggregate);
            repositoryPeer.save(data);
            aggregate.setVersion(data.getVersion());
            aggregate.clearDomainEvents();
        } catch (RepositoryPeerSaveException e) {
            throw new RepositorySaveException(e);
        }
    }

    @Override
    public void delete(T aggregate) {
        repositoryPeer.delete(mapper.toData(aggregate));
    }
}
