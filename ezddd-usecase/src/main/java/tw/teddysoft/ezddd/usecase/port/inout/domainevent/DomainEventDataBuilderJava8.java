package tw.teddysoft.ezddd.usecase.port.inout.domainevent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import tw.teddysoft.ezddd.common.Json;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * {@code DomainEventDataBuilderJava8} is a builder for building
 * {@code DomainEventData} from {@code DomainEvent}.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class DomainEventDataBuilderJava8 {

    private static final JsonMapper mapper = Json.mapper;

    private byte[] payload;
    private byte[] metadata;
    private String eventType;
    private boolean isJson;
    private UUID id;

    public static <A> DomainEventDataBuilderJava8 json(String eventType, A payload) {
        DomainEventDataBuilderJava8 self = new DomainEventDataBuilderJava8();

        try {
            self.payload = mapper.writeValueAsBytes(payload);
            self.isJson = true;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        self.eventType = eventType;

        return self;
    }

    public static DomainEventDataBuilderJava8 binary(String eventType, byte[] payload) {
        DomainEventDataBuilderJava8 self = new DomainEventDataBuilderJava8();

        self.payload = payload;
        self.eventType = eventType;
        self.isJson = false;

        return self;
    }

    public DomainEventDataBuilderJava8 eventId(UUID id) {
        this.id = id;
        return this;
    }

    public <A> DomainEventDataBuilderJava8 metadataAsJson(A value) {
        try {
            this.metadata = mapper.writeValueAsBytes(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return this;
    }

    public DomainEventDataBuilderJava8 metadataAsBytes(byte[] value) {
        this.metadata = value;
        return this;
    }

    public DomainEventData build() {
        UUID eventId = this.id == null ? UUID.randomUUID() : this.id;
        String contentType = this.isJson ? "application/json" : "application/octet-stream";
        byte[] userMetaData = this.metadata == null ? "{}".getBytes(StandardCharsets.UTF_8) : this.metadata;
        return new DomainEventData(eventId, this.eventType, contentType, this.payload, userMetaData);
    }

}
