package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code VersionedInput} is an interface for representing the input with
 * aggregate root version of use case execution.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface VersionedInput extends Input {
    long getVersion();

    void setVersion(long version);
}
