package tw.teddysoft.ezddd.usecase.port.out.idempotent;

public enum EventProcessingStatus {
    ACTIVE, COMMITTED
}
