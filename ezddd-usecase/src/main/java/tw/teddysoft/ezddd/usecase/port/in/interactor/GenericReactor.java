package tw.teddysoft.ezddd.usecase.port.in.interactor;

import java.util.function.Consumer;

public class GenericReactor<Message> implements Reactor<Message> {

    private final Consumer<Message> consumer;

    public GenericReactor(Consumer<Message> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void execute(Message message) {
        consumer.accept(message);
    }
}
 