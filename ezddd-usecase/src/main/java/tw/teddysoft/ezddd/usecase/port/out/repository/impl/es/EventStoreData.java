package tw.teddysoft.ezddd.usecase.port.out.repository.impl.es;

import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.StoreData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * {@code EventStoreData} is an implementation of {@code StoreData} for event
 * sourcing. It contains a list of domain event data of current changes.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class EventStoreData implements StoreData<String> {

    private String streamName;
    private long version;
    private List<DomainEventData> domainEventDatas;

    public EventStoreData() {
        domainEventDatas = new ArrayList<>();
    }

    public EventStoreData(String streamName, long version, List<DomainEventData> domainEventDatas) {
        this.streamName = streamName;
        this.version = version;
        this.domainEventDatas = domainEventDatas;
    }

    public List<DomainEventData> getDomainEventDatas() {
        return domainEventDatas;
    }

    public void setDomainEventDatas(List<DomainEventData> domainEventDatas) {
        this.domainEventDatas = domainEventDatas;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String getId() {
        return streamName;
    }

    @Override
    public void setId(String id) {
        this.streamName = id;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    @Override
    public boolean equals(Object that) {
        if(that instanceof EventStoreData target) {
            return version == target.getVersion() &&
                   streamName.equals(target.getStreamName()) &&
                    Objects.equals(domainEventDatas, target.getDomainEventDatas());

//                   domainEventDatas.equals(target.getDomainEventDatas());
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.version);
        hash = 79 * hash + Objects.hashCode(this.streamName);
        hash = 79 * hash + Objects.hashCode(this.domainEventDatas);
        return hash;
    }
}
