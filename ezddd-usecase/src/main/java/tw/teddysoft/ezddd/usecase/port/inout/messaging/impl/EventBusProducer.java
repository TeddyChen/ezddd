package tw.teddysoft.ezddd.usecase.port.inout.messaging.impl;

import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;

public class EventBusProducer implements MessageProducer<DomainEventData> {
    private final MessageBus<DomainEventData> eventBus;

    public EventBusProducer(MessageBus<DomainEventData> eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void post(DomainEventData domainEventData) {
        eventBus.post(domainEventData);
    }

    @Override
    public void close() throws IOException {
    }
}
