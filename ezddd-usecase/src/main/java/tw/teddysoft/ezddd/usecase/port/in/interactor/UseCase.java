package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code UseCase} is an interface for representing a use case in clean
 * architecture.
 *
 * @param <I> the type parameter for representing a use case input
 * @param <O> the type parameter for representing a use case output
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface UseCase<I extends Input, O extends Output> {
    O execute(I input) throws UseCaseFailureException;
}
