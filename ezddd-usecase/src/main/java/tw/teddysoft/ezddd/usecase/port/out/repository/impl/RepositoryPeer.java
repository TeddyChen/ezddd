package tw.teddysoft.ezddd.usecase.port.out.repository.impl;

import java.util.Optional;

/**
 * {@code RepositoryPeer} is an interface for {@code Repository} to access
 * aggregate with {@code StoreData}.
 *
 * @param <T> the type parameter for representing aggregate data
 * @param <ID> the type parameter for representing aggregate id
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface RepositoryPeer<T extends StoreData, ID> {

    Optional<T> findById(ID id);

    void save(T data);

    void delete(T data);
}
