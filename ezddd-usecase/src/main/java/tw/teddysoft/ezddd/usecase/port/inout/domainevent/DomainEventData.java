package tw.teddysoft.ezddd.usecase.port.inout.domainevent;


import org.json.JSONObject;

import java.util.UUID;

/**
 * {@code DomainEventData} is a persistent object for storing domain events to
 * database or reading from database.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public record DomainEventData(UUID id, String eventType, String contentType, byte[] eventBody, byte[] userMetadata) {

    @Override
    public boolean equals(Object that) {
        if(that instanceof DomainEventData target) {
            var thisBody = new JSONObject(new String(this.eventBody));
            var targetBody = new JSONObject(new String(target.eventBody));
            var thisMetadata = new JSONObject(new String(this.userMetadata));
            var targetMetadata = new JSONObject(new String(target.userMetadata));

            return  this.id.equals(target.id())&&
                    this.eventType.equals(target.eventType()) &&
                    this.contentType.equals(target.contentType()) &&
                    thisBody.similar(targetBody) &&
                    thisMetadata.similar(targetMetadata);
        }
        return false;
    }
}
