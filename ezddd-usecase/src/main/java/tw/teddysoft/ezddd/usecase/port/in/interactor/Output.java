package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code Output} is an interface for representing the output after executing
 * the use case.
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface Output {

    String getMessage();

    Output setMessage(String message);

    ExitCode getExitCode();

    Output setExitCode(ExitCode exitCode);

    Output fail();

    Output succeed();

    String getId();

    Output setId(String id);

}
