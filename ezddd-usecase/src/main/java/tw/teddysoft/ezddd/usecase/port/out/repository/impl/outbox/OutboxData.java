package tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox;

import tw.teddysoft.ezddd.usecase.port.out.repository.impl.StoreData;

/**
 * {@code OutboxData} is an interface of {@code StoreData} for state
 * sourcing. It contains the current state of an aggregate.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface OutboxData<ID> extends StoreData<ID> {
}
