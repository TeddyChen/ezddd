package tw.teddysoft.ezddd.usecase.port.inout.messaging;

import java.io.Closeable;

/**
 * {@link MessageProducer} is an interface for publishing messages to
 * an event bus or event broker.
 * You can publish messages via the {@link #post(Object)} method.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 2.0
 */
public interface MessageProducer<Message> extends Closeable {
    void post(Message message);
}
