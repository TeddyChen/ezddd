package tw.teddysoft.ezddd.usecase.port.out.repository.impl.es;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.EsAggregateRoot;
import tw.teddysoft.ezddd.usecase.port.out.repository.Repository;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeerSaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeer;
import tw.teddysoft.ezddd.usecase.port.out.repository.RepositorySaveException;

import java.util.List;
import java.util.Optional;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

/**
 * {@code EsRepository} is a generic implementation of {@code Repository} for event
 * sourcing.
 *
 * @param <T> the type parameter for representing aggregate root
 * @param <ID> the type parameter for representing aggregate id
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class EsRepository<T extends EsAggregateRoot, ID> implements Repository<T, ID> {

    private final RepositoryPeer<EventStoreData, String> repositoryPeer;
    private final Class clazz;
    private final String category;

    public EsRepository(RepositoryPeer<EventStoreData, String> repositoryPeer, Class clazz, String category) {
        this.repositoryPeer = repositoryPeer;
        this.clazz = clazz;
        this.category = category;
    }

    @Override
    public Optional<T> findById(ID aggregateId) {
        requireNotNull("AggregateId", aggregateId);
        Optional<EventStoreData> aggregateRootData = repositoryPeer.findById(EsAggregateRoot.getStreamName(category, aggregateId.toString()));

        if (aggregateRootData.isEmpty()) {
            return Optional.empty();
        }

        List<DomainEvent> domainEvents = DomainEventMapper.toDomain(aggregateRootData.get().getDomainEventDatas());

        try {
            T aggregate = (T) clazz.getConstructor(List.class).newInstance(domainEvents);
            aggregate.setVersion(aggregateRootData.get().getVersion());
            if (aggregate.isDeleted())
                return Optional.empty();
            else
                return Optional.of(aggregate);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void save(T aggregate) {
        requireNotNull("Aggregate", aggregate);

        try {
            EventStoreData eventStoreData = EventStoreMapper.toData(aggregate);
            repositoryPeer.save(eventStoreData);
            aggregate.setVersion(eventStoreData.getVersion());
            aggregate.clearDomainEvents();
        } catch (RepositoryPeerSaveException e) {
            throw new RepositorySaveException(e);
        }
    }

    @Override
    public void delete(T aggregate) {
        requireNotNull("Aggregate", aggregate);
        EventStoreData eventStoreData = EventStoreMapper.toData(aggregate);
        repositoryPeer.delete(eventStoreData);
    }
}
