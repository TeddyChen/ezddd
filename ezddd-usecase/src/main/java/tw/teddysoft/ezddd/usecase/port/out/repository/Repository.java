package tw.teddysoft.ezddd.usecase.port.out.repository;

import tw.teddysoft.ezddd.entity.AggregateRoot;

import java.util.Optional;

/**
 * {@code Repository} is an interface for the repository pattern in 
 * domain-driven design.
 *
 * @param <T>   the type parameter for an aggregate root
 * @param <ID>  the type parameter for an aggregate id
 *           
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface Repository<T extends AggregateRoot, ID> {

    Optional<T> findById(ID id);

    void save(T data) throws RepositorySaveException;

    void delete(T data);
}
