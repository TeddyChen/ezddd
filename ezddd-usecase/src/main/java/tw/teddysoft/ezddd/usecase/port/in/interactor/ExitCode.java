package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code ExitCode} is an enum for representing the execution status of a
 * use case.
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public enum ExitCode {

    SUCCESS("Success", 0),
    FAILURE("Failure", 1);

    private final String string;
    private final int code;

    ExitCode(String string, int code)
    {
        this.string = string;
        this.code = code;
    }


    @Override
    public String toString(){
        return this.string;
    }

    public int getCode(){
        return this.code;
    }

}
