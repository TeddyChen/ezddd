package tw.teddysoft.ezddd.usecase.port.inout.domainevent;

import edu.emory.mathcs.backport.java.util.Collections;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEvent;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * {@code DomainEventDto} is a class to represent the domain event that
 * transmitted between different bounded contexts. This class is previous
 * known as RemoteDomainEvent.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 2.0
 */
public class DomainEventDto {

	private UUID id;
	private Instant occurredOn;
	private String jsonEvent;
	private String eventType;
	private String eventSimpleName;
	private String boundedContext;
	private Map<String, String> metadata;

	@Override
	public String toString() {
		return "Event Type: " + this.eventType + " ; Event body: " + this.jsonEvent;
	}

	public DomainEventDto(DomainEvent event, String boundedContext, Instant occurredOn)  {
		this(event, UUID.randomUUID(), boundedContext, occurredOn);
	}

	public DomainEventDto(DomainEvent event, UUID id, String boundedContext, Instant occurredOn){
		this(id, occurredOn, Json.asString(event), event.getClass().getTypeName(), event.getClass().getSimpleName(), boundedContext);
	}

	public DomainEventDto(UUID id, Instant occurredOn, String jsonEvent, String eventType, String eventSimpleName, String boundedContext) {
		this();
		this.id = id;
		this.occurredOn = occurredOn;
		this.jsonEvent = jsonEvent;
		this.eventType = eventType;
		this.eventSimpleName = eventSimpleName;
		this.boundedContext = boundedContext;
	}

	// region: For json deserialize
	private DomainEventDto() {
		metadata = new HashMap<>();
	}

	private void setId(UUID id) {
		this.id = id;
	}

	private void setOccurredOn(Instant occurredOn) {
		this.occurredOn = occurredOn;
	}

	private void setJsonEvent(String jsonEvent) {
		this.jsonEvent = jsonEvent;
	}

	private void setEventType(String eventType) {
		this.eventType = eventType;
	}

	private void setEventSimpleName(String eventSimpleName) {
		this.eventSimpleName = eventSimpleName;
	}

	private void setBoundedContext(String boundedContext) {
		this.boundedContext = boundedContext;
	}

	private void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	// endregion
	
	public String getJsonEvent(){
		return jsonEvent;
	}

	public String getEventType(){
		return eventType;
	}

	public String getEventSimpleName() {
		return eventSimpleName;
	}

	public String getBoundedContext() {
		return boundedContext;
	}

	public UUID id() {
		return id;
	}

	public Instant occurredOn() {
		return occurredOn;
	}

	public Map<String, String> getMetadata() {
		return Collections.unmodifiableMap(this.metadata);
	}
}
