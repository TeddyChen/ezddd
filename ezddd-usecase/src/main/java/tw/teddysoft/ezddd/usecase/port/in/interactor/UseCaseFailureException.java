package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code UseCaseFailureException} is the subclass of {@code RuntimeException}.
 * When a use case can not fulfill its specifications,
 * {@code UseCaseFailureException} will be thrown.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class UseCaseFailureException extends RuntimeException{

    public UseCaseFailureException(){
        super();
    }

    public UseCaseFailureException(Exception e){
        super(e);
    }

    public UseCaseFailureException(String message){
        super(message);
    }

    public UseCaseFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}
