package tw.teddysoft.ezddd.usecase.port.out.repository.impl.es;

import tw.teddysoft.ezddd.entity.EsAggregateRoot;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;

/**
 * {@code EventStoreMapper} is a class to map a {@code EsAggregateRoot} to
 * {@code EventStoreData} which stored in event sourcing database.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class EventStoreMapper {

    public static EventStoreData toData(EsAggregateRoot aggregateRoot) {

        EventStoreData eventStoreData = new EventStoreData();
        eventStoreData.setStreamName(aggregateRoot.getStreamName());
        eventStoreData.setVersion(aggregateRoot.getVersion());
        eventStoreData.setDomainEventDatas(DomainEventMapper.toData(aggregateRoot.getDomainEvents()));

        return eventStoreData;
    }
}
