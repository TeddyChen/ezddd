package tw.teddysoft.ezddd.usecase.port.inout.messaging.impl;

import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;

import java.util.ArrayList;
import java.util.List;

public class BlockingMessageBus<Event> implements MessageBus<Event> {

    private final List<Reactor<Event>> reactors;

    public BlockingMessageBus() {
        reactors = new ArrayList<>();
    }

    @Override
    public void register(Reactor<Event> reactor) {
        reactors.add(reactor);
    }

    @Override
    public void unregister(Reactor<Event> reactor) {
        reactors.remove(reactor);
    }

    @Override
    public void post(Event event) {
        for (Reactor<Event> reactor: reactors) {
            reactor.execute(event);
        }
    }
}
