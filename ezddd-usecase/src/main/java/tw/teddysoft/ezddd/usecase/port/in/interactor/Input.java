package tw.teddysoft.ezddd.usecase.port.in.interactor;

/**
 * {@code Input} is a marker interface for representing the input of a use case
 * execution.
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface Input {

    /**
     * @since 1.0.5
     */
    static NullInput ofNull() {
        return new NullInput();
    }

    /**
     * @since 1.0.5
     */
    final class NullInput implements Input {}
}
