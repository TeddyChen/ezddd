package tw.teddysoft.ezddd.usecase.port.in.interactor;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.out.idempotent.IdempotencyService;

/**
 * {@link IdempotentReactor} is an abstract class for the concrete reactors that
 * support idempotency to extend. {@link IdempotentReactor} implements the method
 * {@link #execute(DomainEventData)} as a template method to ignore the events committed
 * to the {@link IdempotencyService} and commit the event to the {@link IdempotencyService}
 * when the event handling is completed. The concrete Reactors must implement
 * the primitive operation {@link #handleEvent(DomainEvent)} to handle the event.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 2.0
 */
public abstract class IdempotentReactor implements Reactor<DomainEventData> {
    protected final IdempotencyService idempotencyService;

    protected IdempotentReactor(IdempotencyService idempotencyService) {
        this.idempotencyService = idempotencyService;
    }

    @Override
    public final void execute(DomainEventData domainEventData) {
        try {
            DomainEvent domainEvent = DomainEventMapper.toDomain(domainEventData);
            if (idempotencyService.isEventCommitted(domainEvent.id().toString())) return;
            handleEvent(domainEvent);
            idempotencyService.commitEventHandling(domainEvent.id().toString());
        } catch (Exception e){
            e.printStackTrace();
            //TODO: to log the exception
        }
    }

    protected abstract void handleEvent(DomainEvent domainEvent);
}
