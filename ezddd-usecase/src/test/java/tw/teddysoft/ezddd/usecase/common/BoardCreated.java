package tw.teddysoft.ezddd.usecase.common;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;

import java.time.Instant;
import java.util.UUID;

public record BoardCreated(String teamId,
                           String boardId,
                           String boardName,
                           UUID id,
                           Instant occurredOn) implements DomainEvent {
    @Override
    public String aggregateId() {
        return boardId;
    }

    public static class TypeMapper extends DomainEventTypeMapper.DefaultMapper {
        public static final String MAPPING_TYPE_PREFIX = "BoardEvents$";
        public static final String BOARD_CREATED = MAPPING_TYPE_PREFIX + "BoardCreated";

        private static final DomainEventTypeMapper mapper;

        static {
            mapper = DomainEventTypeMapper.create();
            mapper.put(BOARD_CREATED, BoardCreated.class);
        }

        public static DomainEventTypeMapper getInstance() {
            return mapper;
        }


    }
    public static DomainEventTypeMapper mapper() {
        return TypeMapper.getInstance();
    }
}
