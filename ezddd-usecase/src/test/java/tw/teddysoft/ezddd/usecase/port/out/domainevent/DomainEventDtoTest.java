package tw.teddysoft.ezddd.usecase.port.out.domainevent;

import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DomainEventDtoTest {
    @Test
    public void should_succeed_when_write_remote_domain_event() {

        UUID id = UUID.randomUUID();
        Instant now = Instant.now();

        BoardCreated boardCreated = new BoardCreated(id, "ScrumBoard", now);
        DomainEventDto remoteBoardCreated = new DomainEventDto(boardCreated, "DDDCORE", now);

        Map<String, String> map = Json.readValue(remoteBoardCreated.getJsonEvent(), Map.class);
        Instant.parse(map.get("occurredOn"));

        assertEquals("BoardCreated", remoteBoardCreated.getEventSimpleName());
        assertEquals("tw.teddysoft.ezddd.usecase.port.out.domainevent.DomainEventDtoTest$BoardCreated", remoteBoardCreated.getEventType());
        assertEquals("DDDCORE", remoteBoardCreated.getBoundedContext());
        assertEquals(id.toString(), map.get("id"));
        assertEquals(now, Instant.parse(map.get("occurredOn")));
        assertEquals("ScrumBoard", map.get("boardName"));

        assertEquals(Json.asString(boardCreated), remoteBoardCreated.getJsonEvent());
    }


    record BoardCreated(
            UUID id,
            String boardName,
            Instant occurredOn
    ) implements DomainEvent {
        @Override
        public String aggregateId() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
