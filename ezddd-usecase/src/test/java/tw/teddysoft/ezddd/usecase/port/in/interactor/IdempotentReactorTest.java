package tw.teddysoft.ezddd.usecase.port.in.interactor;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.usecase.common.BoardCreated;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.out.idempotent.IdempotencyService;


import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class IdempotentReactorTest {

    @BeforeAll
    public static void beforeAll() {
        DomainEventMapper.setMapper(BoardCreated.mapper());
    }

    @Test
    public void the_event_should_be_committed_after_handling() {
        // Given "a IdempotentReactor"
        IdempotencyService idempotencyService = new FakeIdempotencyService();
        IdempotentReactor reactor = new CorrectIdempotentReactor(idempotencyService);
        // When "the Reactor handles an event"
        BoardCreated event = new BoardCreated(
                "teamId",
                "boardId",
                "boardName",
                UUID.randomUUID(),
                Instant.now()
        );
        reactor.execute(DomainEventMapper.toData(event));
        // Then "the event should be committed"
        assertTrue(idempotencyService.isEventCommitted(event.id().toString()));

    }

    @Test
    public void the_event_should_not_be_committed_if_handling_failed() {
        // Given "a IdempotentReactor"
        IdempotencyService idempotencyService = new FakeIdempotencyService();
        IdempotentReactor reactor = new IncorrectIdempotentReactor(idempotencyService);

        // When "the Reactor fails to handle the event"
        BoardCreated event = new BoardCreated(
                "teamId",
                "boardId",
                "boardName",
                UUID.randomUUID(),
                Instant.now()
        );

        reactor.execute(DomainEventMapper.toData(event));

        // Then "the event should not be committed"
        assertFalse(idempotencyService.isEventCommitted(event.id().toString()));
    }

    @Test
    public void the_active_event_should_be_handled() {
        // Given "a IdempotentReactor"
        IdempotencyService idempotencyService = new FakeIdempotencyService();
        IdempotentReactor idempotentReactor = new FirstTimeFailedReactor(idempotencyService);

        // And "the reactor activates an event and fails"
        BoardCreated event = new BoardCreated(
            "teamId",
            "boardId",
            "boardName",
            UUID.randomUUID(),
            Instant.now()
        );

        idempotentReactor.execute(DomainEventMapper.toData(event));

        assertTrue(idempotencyService.isEventActive(event.id().toString()));

        // When "the Reactor receives the same event and succeed"
        idempotentReactor.execute(DomainEventMapper.toData(event));

        // Then "the event should be committed"
        assertTrue(idempotencyService.isEventCommitted(event.id().toString()));
    }

    @Test
    public void the_committed_event_should_be_ignored() {
        // Given "a IdempotentReactor"
        IdempotencyService idempotencyService = new FakeIdempotencyService();
        IdempotentReactor idempotentReactor = new CorrectIdempotentReactor(idempotencyService);

        // And "the reactor handles an event successfully"
        BoardCreated event = new BoardCreated(
                "teamId",
                "boardId",
                "boardName",
                UUID.randomUUID(),
                Instant.now()
        );
        idempotentReactor.execute(DomainEventMapper.toData(event));
        assertEquals(1, ((CorrectIdempotentReactor) idempotentReactor).domainEvents.size());

        // When "the reactor receives the same event"
        idempotentReactor.execute(DomainEventMapper.toData(event));

        // Then "the event should be ignored"
        assertEquals(1, ((CorrectIdempotentReactor) idempotentReactor).domainEvents.size());
    }

    class CorrectIdempotentReactor extends IdempotentReactor {
        public List<DomainEvent> domainEvents = new ArrayList<>();

        CorrectIdempotentReactor(IdempotencyService idempotencyService) {
            super(idempotencyService);
        }

        @Override
        protected void handleEvent(DomainEvent domainEvent) {
            domainEvents.add(domainEvent);
        }
    }

    class IncorrectIdempotentReactor extends IdempotentReactor {
        IncorrectIdempotentReactor(IdempotencyService idempotencyService) {
            super(idempotencyService);
        }

        @Override
        protected void handleEvent(DomainEvent domainEvent) {
            throw new RuntimeException();
        }
    }


    private class FirstTimeFailedReactor extends IdempotentReactor {
        public FirstTimeFailedReactor(IdempotencyService idempotencyService) {
            super(idempotencyService);
        }

        @Override
        protected void handleEvent(DomainEvent domainEvent) {
            if (!idempotencyService.isEventActive(domainEvent.id().toString())) {
                idempotencyService.activateEventHandling(domainEvent.id().toString());
                throw new RuntimeException();
            }
        }

    }
}
