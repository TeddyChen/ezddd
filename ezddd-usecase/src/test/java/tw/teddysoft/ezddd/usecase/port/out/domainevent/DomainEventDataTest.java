package tw.teddysoft.ezddd.usecase.port.out.domainevent;

import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DomainEventDataTest {
    @Test
    public void equals_return_false_when_not_domain_event_data() {
        var domainEventData = new DomainEventData(UUID.randomUUID(), "UserRegistered", "email", null, null);
        assertFalse(domainEventData.equals("UserRegistered"));
    }

    @Test
    public void equals_return_true() {
        var uuid = UUID.randomUUID();
        byte[] byteArray = "{}".getBytes();
        var domainEventData = new DomainEventData(uuid, "UserRegistered", "email", byteArray, byteArray);
        var domainEventData2 = new DomainEventData(uuid, "UserRegistered", "email", byteArray, byteArray);
        assertTrue(domainEventData.equals(domainEventData2));
    }
}
