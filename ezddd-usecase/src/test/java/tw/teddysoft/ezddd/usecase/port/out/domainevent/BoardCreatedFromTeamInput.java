package tw.teddysoft.ezddd.usecase.port.out.domainevent;

import tw.teddysoft.ezddd.usecase.port.in.interactor.Input;

public class BoardCreatedFromTeamInput implements Input {
    public String teamId;
    public String boardId;
    public String boardName;
}
