package tw.teddysoft.ezddd.usecase.port.in.interactor;

import tw.teddysoft.ezddd.usecase.port.out.idempotent.EventProcessingStatus;
import tw.teddysoft.ezddd.usecase.port.out.idempotent.IdempotencyService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FakeIdempotencyService implements IdempotencyService {
    private final Map<String, String> idempotentTable = new HashMap<>();

    @Override
    public Optional<EventProcessingStatus> getEventHandlingStatus(String eventId) {
        if (idempotentTable.get(eventId) == null) {
            return Optional.empty();
        }
        return Optional.of(EventProcessingStatus.valueOf(idempotentTable.get(eventId)));
    }

    @Override
    public boolean isEventHandled(String eventId) {
        return idempotentTable.containsKey(eventId);
    }

    @Override
    public void activateEventHandling(String eventId) {
        idempotentTable.put(eventId, EventProcessingStatus.ACTIVE.toString());
    }

    @Override
    public void commitEventHandling(String eventId) {
        idempotentTable.put(eventId, EventProcessingStatus.COMMITTED.toString());
    }
}
