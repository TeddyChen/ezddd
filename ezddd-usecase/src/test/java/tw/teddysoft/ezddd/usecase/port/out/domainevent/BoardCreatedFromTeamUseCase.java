package tw.teddysoft.ezddd.usecase.port.out.domainevent;

import tw.teddysoft.ezddd.usecase.port.in.interactor.Output;
import tw.teddysoft.ezddd.usecase.port.in.interactor.UseCase;
import tw.teddysoft.ezddd.usecase.port.in.interactor.UseCaseFailureException;

public class BoardCreatedFromTeamUseCase implements UseCase<BoardCreatedFromTeamInput, Output> {
    @Override
    public Output execute(BoardCreatedFromTeamInput input) throws UseCaseFailureException {
        System.out.println("teamId: " + input.teamId + ", boardId: " + input.boardId + ", boardName: " + input.boardName);
        return null;
    }
}
