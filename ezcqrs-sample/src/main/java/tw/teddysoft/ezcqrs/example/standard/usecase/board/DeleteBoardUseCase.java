package tw.teddysoft.ezcqrs.example.standard.usecase.board;

import tw.teddysoft.ezcqrs.example.standard.entity.board.Board;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;

public class DeleteBoardUseCase implements Command<DeleteBoardInput, CqrsOutput> {

    private final Repository<Board, String> boardRepository;

    public DeleteBoardUseCase(Repository<Board, String> boardRepository) {
        this.boardRepository = boardRepository;
    }

    @Override
    public CqrsOutput execute(DeleteBoardInput input) {
        CqrsOutput output = CqrsOutput.create();
        if (boardRepository.findById(input.getBoardId()).isPresent()) {
            Board board = boardRepository.findById(input.getBoardId()).get();
            board.markAsDeleted();
            boardRepository.delete(board);
            output.setId(input.getBoardId());
            output.setExitCode(ExitCode.SUCCESS);

            return output;
        }
        output.setExitCode(ExitCode.FAILURE);
        return output;
    }
}
