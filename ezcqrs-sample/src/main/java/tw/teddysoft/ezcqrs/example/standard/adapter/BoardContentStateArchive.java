package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.BoardContentState;
import tw.teddysoft.ezddd.cqrs.usecase.query.Archive;

import java.util.Optional;

public class BoardContentStateArchive implements Archive<BoardContentState, String> {
    private final InMemoryQueryStore queryStore;

    public BoardContentStateArchive(InMemoryQueryStore queryStore) {
        this.queryStore = queryStore;
    }

    @Override
    public Optional<BoardContentState> findById(String id) {
        Optional<Object> result = queryStore.findById(id);
        return result.isPresent() ? Optional.of((BoardContentState) result.get()) : Optional.empty();
    }

    @Override
    public void save(BoardContentState data) {
        queryStore.save(data.boardState().getBoardId(), data);
    }

    @Override
    public void delete(BoardContentState data) {
        queryStore.delete(data.boardState().getBoardId());

    }
}
