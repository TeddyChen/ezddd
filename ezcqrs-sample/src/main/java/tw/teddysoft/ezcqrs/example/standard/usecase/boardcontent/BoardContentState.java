package tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent;

import tw.teddysoft.ezcqrs.example.standard.adapter.BoardState;
import tw.teddysoft.ezcqrs.example.standard.adapter.WorkflowState;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WipLimit;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BoardContentState {
    private final BoardState boardState;
    private final List<WorkflowState> workflowStates;

    public BoardContentState() {
        boardState = new BoardState();
        workflowStates = new ArrayList<>();
    }

    public BoardState boardState() {
        return boardState;
    }

    public List<WorkflowState> workflowStates() {
        return workflowStates;
    }
}
