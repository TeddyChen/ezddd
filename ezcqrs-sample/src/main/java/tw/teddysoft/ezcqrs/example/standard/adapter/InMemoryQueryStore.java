package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.BoardContentState;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryQueryStore {
    private final Map<String, Object> states;

    public InMemoryQueryStore() {
        this.states = new HashMap<>();
    }

    public void save(String id, Object data){
        states.put(id, data);
    }

    public Optional<Object> findById(String id){
        return Optional.ofNullable(states.get(id));
    }

    public void delete(String id){
        states.remove(id);
    }
}
