package tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent;

import tw.teddysoft.ezcqrs.example.standard.adapter.BoardContentStateArchive;
import tw.teddysoft.ezcqrs.example.standard.adapter.WorkflowState;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardCreated;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardDeleted;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowCreated;
import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.cqrs.usecase.query.Projector;

import java.util.ArrayList;

public class BoardContentStateProjector implements Projector {
    private final BoardContentStateArchive archive;

    public BoardContentStateProjector(BoardContentStateArchive archive) {
        this.archive = archive;
    }

    public void project(DomainEvent event) {
        if (event instanceof BoardCreated e) {
            BoardContentState boardContentState = new BoardContentState();
            boardContentState.boardState().setBoardId(e.boardId());
            boardContentState.boardState().setName(e.boardName());
            boardContentState.boardState().setVersion(-1);
            archive.save(boardContentState);
        } else if (event instanceof WorkflowCreated e) {
            BoardContentState boardContentState = archive.findById(e.boardId()).get();
            WorkflowState workflowState = new WorkflowState();
            workflowState.setBoardId(e.boardId());
            workflowState.setWorkflowId(e.workflowId());
            workflowState.setName(e.workflowName());
            workflowState.setStages(new ArrayList<>());
            workflowState.setVersion(-1);
            boardContentState.workflowStates().add(workflowState);
            archive.save(boardContentState);
        } else if (event instanceof BoardDeleted e) {
            BoardContentState boardContentState = archive.findById(e.boardId()).get();
            archive.delete(boardContentState);
        }
    }
}
