package tw.teddysoft.ezcqrs.example.standard.usecase.board;

import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventData;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxData;

import java.util.List;

public class BoardData implements OutboxData {

    private static final String CATEGORY = "Board";

    private String id;
    private String name;
    private long version;
    private List<DomainEventData> domainEventDatas;

    @Override
    public long getVersion() {
        return version;
    }

    @Override
    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public List<DomainEventData> getDomainEventDatas() {
        return domainEventDatas;
    }

    @Override
    public void setDomainEventDatas(List<DomainEventData> domainEventDatas) {
        this.domainEventDatas = domainEventDatas;
    }

    @Override
    public String getStreamName() {
        return CATEGORY + "-" + id;
    }

    @Override
    public void setStreamName(String streamName) {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
