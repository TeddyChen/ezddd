package tw.teddysoft.ezcqrs.example.standard.usecase.workflow;

import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;

public class CreateWorkflowUseCase implements Command<CreateWorkflowInput, CqrsOutput> {
    private final Repository<Workflow, String> repository;

    public CreateWorkflowUseCase(Repository<Workflow, String> repository) {
        this.repository = repository;
    }

    public CqrsOutput execute(CreateWorkflowInput input) {
        Workflow workflow = new Workflow(input.getBoardId(), input.getWorkflowId(), input.getWorkflowName());
        repository.save(workflow);

        CqrsOutput output = new CqrsOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);
        return output;
    }
}
