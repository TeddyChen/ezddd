package tw.teddysoft.ezcqrs.example.standard.entity.workflow;

import tw.teddysoft.ezddd.core.entity.AggregateRoot;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Workflow extends AggregateRoot {
    private String boardId;
    private String workflowId;
    private String name;
    private List<Stage> stages;

    public Workflow(String boardId, String workflowId, String name) {
        super();
        this.name = name;
        this.workflowId = workflowId;
        this.boardId = boardId;
        stages = new ArrayList<>();

        apply(new WorkflowCreated(this.boardId, this.workflowId, this.name, UUID.randomUUID(), Instant.now()));
    }

    public void markAsDeleted() {
        isDeleted = true;
        apply(new WorkflowDeleted(this.workflowId, UUID.randomUUID(), Instant.now()));
    }

    @Override
    public String getId() {
        return workflowId;
    }

    public String getName() {
        return name;
    }

    public String getBoardId() {
        return boardId;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void createStage(String stageId, String stageName, WipLimit wipLimit) {
        Stage stage = new Stage(stageId, stageName, wipLimit);
        stages.add(stage);
        apply(new StageCreated(this.workflowId, stageId, stageName, wipLimit, UUID.randomUUID(), Instant.now()));
    }
}
