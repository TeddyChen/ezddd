package tw.teddysoft.ezcqrs.example.standard.adapter;

import org.json.JSONException;
import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;

import java.util.concurrent.TimeUnit;

public class OutboxStoreListener implements Runnable {
    private final DomainEventBus domainEventBus;
    private boolean keepRunning;
    private final InMemoryOutboxStore outboxStore;
    private int currentIndex;

    // MILLISECONDS
    private int pollingInterval;

    public OutboxStoreListener(int pollingInterval, DomainEventBus domainEventBus, InMemoryOutboxStore outboxStore){
        this.pollingInterval = pollingInterval;
        this.domainEventBus = domainEventBus;
        keepRunning = true;
        this.outboxStore = outboxStore;
        currentIndex = -1;
    }

    @Override
    public void run() {

        while (keepRunning) {
            try {
                if (currentIndex < outboxStore.getDomainEvents().size()-1) {
                    for (DomainEvent domainEvent: outboxStore.getDomainEvents().subList(currentIndex+1, outboxStore.getDomainEvents().size())) {
                        domainEventBus.post(domainEvent);
                        currentIndex++;
                    }
                }

                TimeUnit.MILLISECONDS.sleep(pollingInterval);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException ie) {
                keepRunning = false;
                Thread.currentThread().interrupt();
            }
        }
    }

    public void shutdown(){
        keepRunning = false;
        Thread.currentThread().interrupt();
    }
}

