package tw.teddysoft.ezcqrs.example.standard.entity.board;

import tw.teddysoft.ezddd.core.entity.DomainEventTypeMapper;

public class BoardEventTypeMapper extends DomainEventTypeMapper.DefaultMapper {

    public static final String MAPPING_TYPE_PREFIX = "BoardEvents$";
    public static final String BOARD_CREATED = MAPPING_TYPE_PREFIX + "BoardCreated";
    public static final String BOARD_DELETED = MAPPING_TYPE_PREFIX + "BoardDeleted";

    private static final DomainEventTypeMapper mapper;

    static {
        mapper = DomainEventTypeMapper.create();
        mapper.put(BOARD_CREATED, BoardCreated.class);
        mapper.put(BOARD_DELETED, BoardDeleted.class);
    }

    public static DomainEventTypeMapper getInstance(){
        return mapper;
    }


}
