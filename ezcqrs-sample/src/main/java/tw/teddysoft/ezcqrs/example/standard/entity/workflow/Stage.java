package tw.teddysoft.ezcqrs.example.standard.entity.workflow;

import tw.teddysoft.ezddd.core.entity.Entity;

public class Stage implements Entity<String> {
    private String stageId;
    private String stageName;
    private WipLimit wipLimit;

    public Stage(String stageId, String stageName, WipLimit wipLimit) {
        this.stageId = stageId;
        this.stageName = stageName;
        this.wipLimit = wipLimit;
    }

    @Override
    public String getId() {
        return stageId;
    }

    public String getStageName() {
        return stageName;
    }

    public WipLimit getWipLimit() {
        return wipLimit;
    }
}
