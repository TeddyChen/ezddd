package tw.teddysoft.ezcqrs.example.standard.usecase.workflow;

import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;

public class DeleteWorkflowUseCase implements Command<DeleteWorkflowInput, CqrsOutput> {
    private final Repository<Workflow, String> repository;

    public DeleteWorkflowUseCase(Repository<Workflow, String> repository) {
        this.repository = repository;
    }

    public CqrsOutput execute(DeleteWorkflowInput input) {
        Workflow workflow = repository.findById(input.getWorkflowId()).get();

        workflow.markAsDeleted();
        repository.delete(workflow);

        CqrsOutput output = new CqrsOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);

        return output;
    }

}
