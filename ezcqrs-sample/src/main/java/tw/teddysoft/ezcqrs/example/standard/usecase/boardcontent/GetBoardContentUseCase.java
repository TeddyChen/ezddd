package tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent;

import tw.teddysoft.ezcqrs.example.standard.adapter.BoardContentProjection;
import tw.teddysoft.ezcqrs.example.standard.adapter.BoardContentViewModel;
import tw.teddysoft.ezddd.cqrs.usecase.query.Query;
import tw.teddysoft.ezddd.core.usecase.ExitCode;

public class GetBoardContentUseCase implements Query<GetBoardContentInput, GetBoardContentOutput> {

    private final BoardContentProjection boardContentProjection;

    public GetBoardContentUseCase(BoardContentProjection boardContentProjection) {
        this.boardContentProjection = boardContentProjection;
    }

    @Override
    public GetBoardContentOutput execute(GetBoardContentInput input) {
        GetBoardContentProjectionInput getBoardContentProjectionInput = new GetBoardContentProjectionInput();
        getBoardContentProjectionInput.setBoardId(input.getBoardId());

        GetBoardContentOutput output = GetBoardContentOutput.create();
        if (boardContentProjection.query(getBoardContentProjectionInput).isPresent()) {
            BoardContentViewModel boardContentViewModel = boardContentProjection.query(getBoardContentProjectionInput).get();
            output.setBoardContentViewModel(boardContentViewModel);
            output.setExitCode(ExitCode.SUCCESS);

            return output;
        }
        output.setExitCode(ExitCode.FAILURE);
        return output;
    }
}
