package tw.teddysoft.ezcqrs.example.standard.entity.board;

import tw.teddysoft.ezddd.core.entity.AggregateRoot;
import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public class Board extends AggregateRoot<String, DomainEvent> {

    private String id;
    private String name;

    public Board(String id, String name) {
        super();
        this.id = id;
        this.name = name;

        apply(new BoardCreated(id, name, UUID.randomUUID(), Instant.now()));
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void markAsDeleted() {
        apply(new BoardDeleted(id, UUID.randomUUID(), Instant.now()));
    }
}
