package tw.teddysoft.ezcqrs.example.standard.entity.board;

import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public record BoardDeleted(String boardId, UUID id, Instant occurredOn) implements DomainEvent.DestructionEvent {

    @Override
    public String aggregateId() {
        return boardId;
    }
}
