package tw.teddysoft.ezcqrs.example.standard.usecase.board;

import tw.teddysoft.ezcqrs.example.standard.entity.board.Board;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;

public class BoardOutboxMapper implements OutboxMapper<Board, BoardData> {
    @Override
    public Board toDomain(BoardData data) {
        Board board = new Board(data.getId(), data.getName());

        board.clearDomainEvents();
        return board;
    }

    @Override
    public BoardData toData(Board aggregateRoot) {
        BoardData boardData = new BoardData();

        boardData.setId(aggregateRoot.getId());
        boardData.setName(aggregateRoot.getName());
        boardData.setVersion(aggregateRoot.getVersion());
        boardData.setDomainEventDatas(DomainEventMapper.toData(aggregateRoot.getDomainEvents()));

        return boardData;
    }
}
