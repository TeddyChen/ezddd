package tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent;

import tw.teddysoft.ezddd.core.usecase.Input;

public class GetBoardContentInput implements Input {

    private String boardId;

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getBoardId() {
        return boardId;
    }
}
