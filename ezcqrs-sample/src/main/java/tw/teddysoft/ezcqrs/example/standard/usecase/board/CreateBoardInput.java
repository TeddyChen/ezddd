package tw.teddysoft.ezcqrs.example.standard.usecase.board;

import tw.teddysoft.ezddd.core.usecase.Input;

public class CreateBoardInput implements Input {

    private String id;
    private String name;

    public String getBoardId() {
        return id;
    }

    public String getBoardName() {
        return name;
    }

    public void setBoardId(String id) {
        this.id = id;
    }

    public void setBoardName(String name) {
        this.name = name;
    }

}
