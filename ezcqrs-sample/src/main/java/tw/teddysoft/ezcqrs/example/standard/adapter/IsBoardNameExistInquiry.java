package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezddd.cqrs.usecase.command.Inquiry;

public class IsBoardNameExistInquiry implements Inquiry<String, Boolean> {
    private final InMemoryOutboxStore outboxStore;

    public IsBoardNameExistInquiry(InMemoryOutboxStore outboxStore) {
        this.outboxStore = outboxStore;
    }

    @Override
    public Boolean query(String boardName) {
        return outboxStore.getAllStates()
                .stream()
                .filter(x -> x instanceof BoardState)
                .map(x -> (BoardState)x)
                .anyMatch(x -> x.getName().equals(boardName));
    }
}
