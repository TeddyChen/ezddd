package tw.teddysoft.ezcqrs.example.standard.entity.board;

import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public record BoardCreated(String boardId, String boardName, UUID id, Instant occurredOn) implements DomainEvent.ConstructionEvent {
    @Override
    public String aggregateId() {
        return boardId;
    }
}
