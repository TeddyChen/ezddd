package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezcqrs.example.standard.adapter.WorkflowState;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;

import java.util.List;
import java.util.Map;

public class BoardContentViewModel {

    private String boardId;
    private String boardName;
    private List<WorkflowState> workflows;

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public void setWorkflows(List<WorkflowState> workflows) {
        this.workflows = workflows;
    }

    public String getBoardId() {
        return boardId;
    }

    public String getBoardName() {
        return boardName;
    }

    public List<WorkflowState> getWorkflows() {
        return workflows;
    }

}
