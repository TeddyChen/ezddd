package tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent;

import tw.teddysoft.ezddd.cqrs.usecase.query.ProjectionInput;

public class GetBoardContentProjectionInput implements ProjectionInput {

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    private String boardId;
}
