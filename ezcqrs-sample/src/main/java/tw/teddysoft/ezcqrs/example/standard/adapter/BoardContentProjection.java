package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.BoardContentState;
import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.GetBoardContentProjectionInput;
import tw.teddysoft.ezddd.cqrs.usecase.query.Projection;

import java.util.ArrayList;
import java.util.Optional;

public class BoardContentProjection implements Projection<GetBoardContentProjectionInput, Optional<BoardContentViewModel>> {

    private final InMemoryQueryStore queryStore;

    public BoardContentProjection(InMemoryQueryStore queryStore) {
        this.queryStore = queryStore;
    }

    @Override
    public Optional<BoardContentViewModel> query(GetBoardContentProjectionInput input) {
        if (queryStore.findById(input.getBoardId()).isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(buildViewModel((BoardContentState) queryStore.findById(input.getBoardId()).get()));
    }

    private BoardContentViewModel buildViewModel(BoardContentState state) {
        BoardContentViewModel boardContentViewModel = new BoardContentViewModel();
        boardContentViewModel.setBoardId(state.boardState().getBoardId());
        boardContentViewModel.setBoardName(state.boardState().getName());

        boardContentViewModel.setWorkflows(new ArrayList<>(state.workflowStates()));

        return boardContentViewModel;
    }
}
