package tw.teddysoft.ezcqrs.example.standard.usecase.workflow;

import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WipLimit;
import tw.teddysoft.ezddd.core.usecase.Input;

public class CreateStageInput implements Input {
    private String workflowId;
    private String stageId;
    private String stageName;
    private WipLimit wipLimit;

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public void setWipLimit(WipLimit wipLimit) {
        this.wipLimit = wipLimit;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public String getStageId() {
        return stageId;
    }

    public String getStageName() {
        return stageName;
    }

    public WipLimit getWipLimit() {
        return wipLimit;
    }
}
