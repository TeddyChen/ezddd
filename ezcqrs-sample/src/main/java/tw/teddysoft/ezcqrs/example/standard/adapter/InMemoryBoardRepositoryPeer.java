package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezcqrs.example.standard.usecase.board.BoardData;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;

import java.util.Optional;

public class InMemoryBoardRepositoryPeer implements RepositoryPeer<BoardData, String> {

    private final InMemoryOutboxStore outboxStore;

    public InMemoryBoardRepositoryPeer(InMemoryOutboxStore outboxStore) {
        this.outboxStore = outboxStore;
    }

    @Override
    public Optional<BoardData> findById(String id) {
        if (outboxStore.findById(id).isEmpty()) {
            return Optional.empty();
        }
        BoardState state = (BoardState)outboxStore.findById(id).get();
        BoardData data = new BoardData();
        data.setId(state.getBoardId());
        data.setName(state.getName());
        data.setVersion(state.getVersion());
        return Optional.of(data);
    }


    @Override
    public void save(BoardData data) {
        outboxStore.saveState(data.getId(), toState(data));
        outboxStore.saveDomainEvent(data.getStreamName(), data.getDomainEventDatas());
    }

    @Override
    public void delete(BoardData data) {
        outboxStore.delete(data.getId());
        outboxStore.saveDomainEvent(data.getStreamName(), data.getDomainEventDatas());
    }

    private BoardState toState(BoardData boardData) {

        BoardState boardState = new BoardState();

        boardState.setName(boardData.getName());
        boardState.setVersion(boardData.getVersion());
        boardState.setBoardId(boardData.getId());

        return boardState;
    }
}
