package tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent;

import tw.teddysoft.ezcqrs.example.standard.adapter.BoardContentViewModel;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class GetBoardContentOutput extends CqrsOutput<GetBoardContentOutput> {

    private BoardContentViewModel boardContentViewModel;

    public void setBoardContentViewModel(BoardContentViewModel boardContentViewModel) {
        this.boardContentViewModel = boardContentViewModel;
    }

    public BoardContentViewModel getBoardContentViewModel() {
        return boardContentViewModel;
    }

    public static GetBoardContentOutput create() {
        return new GetBoardContentOutput();
    }
}
