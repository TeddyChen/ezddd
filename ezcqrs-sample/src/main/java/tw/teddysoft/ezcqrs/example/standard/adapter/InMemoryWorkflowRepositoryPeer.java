package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.WorkflowData;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;

import java.util.*;

public class InMemoryWorkflowRepositoryPeer implements RepositoryPeer<WorkflowData, String> {

    private final InMemoryOutboxStore outboxStore;

    public InMemoryWorkflowRepositoryPeer(InMemoryOutboxStore outboxStore) {
        this.outboxStore = outboxStore;
    }

    @Override
    public Optional<WorkflowData> findById(String workflowId) {

        if(outboxStore.findById(workflowId).isEmpty()){
            return Optional.empty();
        }
        WorkflowState state = (WorkflowState)outboxStore.findById(workflowId).get();
        WorkflowData workflowData = new WorkflowData();

        workflowData.setId(state.getWorkflowId());
        workflowData.setName(state.getName());
        workflowData.setVersion(state.getVersion());
        workflowData.setBoardId(state.getBoardId());
        workflowData.setStages(new ArrayList<>(state.getStages()));

        return Optional.of(workflowData);
    }

    @Override
    public void save(WorkflowData data) {
        outboxStore.saveState(data.getId(), toState(data));
        outboxStore.saveDomainEvent(data.getStreamName(), data.getDomainEventDatas());
    }

    @Override
    public void delete(WorkflowData data) {
        outboxStore.delete(data.getId());
        outboxStore.saveDomainEvent(data.getStreamName(), data.getDomainEventDatas());
    }

    private WorkflowState toState(WorkflowData workflowData) {

        WorkflowState workflowState = new WorkflowState();

        workflowState.setName(workflowData.getName());
        workflowState.setStages(workflowData.getStages());
        workflowState.setVersion(workflowData.getVersion());
        workflowState.setBoardId(workflowData.getBoardId());
        workflowState.setWorkflowId(workflowData.getId());

        return workflowState;
    }
}
