package tw.teddysoft.ezcqrs.example.standard.usecase.workflow;

import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;

public class CreateStageUseCase implements Command<CreateStageInput, CqrsOutput> {
    private final Repository<Workflow, String> workflowRepository;

    public CreateStageUseCase(Repository<Workflow, String> workflowRepository) {
        this.workflowRepository = workflowRepository;
    }

    public CqrsOutput execute(CreateStageInput input) {
        Workflow workflow = workflowRepository.findById(input.getWorkflowId()).get();
        workflow.createStage(input.getStageId(), input.getStageName(), input.getWipLimit());
        workflowRepository.save(workflow);
        CqrsOutput output = new CqrsOutput();
        output.setId(workflow.getId());
        output.setExitCode(ExitCode.SUCCESS);
        return output;
    }
}
