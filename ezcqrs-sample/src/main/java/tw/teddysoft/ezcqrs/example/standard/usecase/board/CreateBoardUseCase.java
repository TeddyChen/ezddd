package tw.teddysoft.ezcqrs.example.standard.usecase.board;

import tw.teddysoft.ezcqrs.example.standard.entity.board.Board;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.cqrs.usecase.command.Inquiry;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;

public class CreateBoardUseCase implements Command<CreateBoardInput, CqrsOutput> {

    private final Repository<Board, String> repository;
    private Inquiry<String, Boolean> inquiry;

    public CreateBoardUseCase(Repository<Board, String> repository, Inquiry<String, Boolean> inquiry) {
        this.repository = repository;
        this.inquiry = inquiry;
    }

    @Override
    public CqrsOutput execute(CreateBoardInput input) {
        CqrsOutput output = CqrsOutput.create();

        if (inquiry.query(input.getBoardName())) {
            output.setExitCode(ExitCode.FAILURE);
            output.setMessage("board name " + input.getBoardName() + " is already used");
            return output;
        }

        Board board = new Board(input.getBoardId(), input.getBoardName());
        repository.save(board);
        output.setId(board.getId());
        output.setExitCode(ExitCode.SUCCESS);

        return output;
    }
}
