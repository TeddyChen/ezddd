package tw.teddysoft.ezcqrs.example.standard.adapter;

import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventData;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryOutboxStore {
    private final Map<String, Object> states;
    private final Map<String, List<DomainEventData>> domainEventDatas;

    public InMemoryOutboxStore() {
        states = new HashMap<>();
        domainEventDatas = new HashMap<>();
    }

    public synchronized void saveState(String id, Object data) {
        states.put(id, data);
    }

    public synchronized void saveDomainEvent(String streamName, List<DomainEventData> domainEventData) {
        if (domainEventDatas.containsKey(streamName)) {
            domainEventDatas.get(streamName).addAll(domainEventData);
        } else {
            domainEventDatas.put(streamName, domainEventData);
        }
    }

    public synchronized void delete(String id) {
        states.remove(id);
    }

    public synchronized Optional<Object> findById(String id) {
        return Optional.ofNullable(states.get(id));
    }

    public synchronized List<DomainEvent> getDomainEvents() {
        List<DomainEvent> domainEvents = domainEventDatas.values()
                .stream()
                .flatMap(x -> x.stream())
                .map(x -> (DomainEvent) DomainEventMapper.toDomain(x))
                .collect(Collectors.toList());
        domainEvents.sort(Comparator.comparing(DomainEvent::occurredOn));

        return domainEvents;
    }

    public synchronized Collection<Object> getAllStates() {
        return states.values();
    }
}
