package tw.teddysoft.ezcqrs.example.standard.usecase.workflow;

import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Stage;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventData;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxData;

import java.util.List;

public class WorkflowData implements OutboxData {

    private static final String CATEGORY = "Workflow";

    private long version;
    private List<DomainEventData> domainEventDatas;
    private String boardId;
    private String workflowId;
    private String name;
    private List<Stage> stages;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    @Override
    public long getVersion() {
        return version;
    }

    @Override
    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String getId() {
        return workflowId;
    }

    @Override
    public void setId(String id) {
        this.workflowId = id;
    }

    @Override
    public List<DomainEventData> getDomainEventDatas() {
        return domainEventDatas;
    }

    @Override
    public void setDomainEventDatas(List<DomainEventData> domainEventDatas) {
        this.domainEventDatas = domainEventDatas;
    }

    @Override
    public String getStreamName() {
        return CATEGORY + "-" + getId();
    }

    @Override
    public void setStreamName(String streamName) {
    }
}
