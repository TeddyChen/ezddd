package tw.teddysoft.ezcqrs.example.standard.usecase.workflow;

import tw.teddysoft.ezddd.core.usecase.Input;

public class CreateWorkflowInput implements Input {
    private String boardId;
    private String workflowId;
    private String workflowName;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }
}
