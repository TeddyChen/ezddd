package tw.teddysoft.ezcqrs.example.standard.entity.workflow;

import tw.teddysoft.ezddd.core.entity.ValueObject;

public record WipLimit(int i) implements ValueObject {

    public static WipLimit valueOf(int value) { return new WipLimit(value); }
}
