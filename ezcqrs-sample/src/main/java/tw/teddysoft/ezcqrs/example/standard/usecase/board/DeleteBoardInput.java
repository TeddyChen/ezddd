package tw.teddysoft.ezcqrs.example.standard.usecase.board;

import tw.teddysoft.ezddd.core.usecase.Input;

public class DeleteBoardInput implements Input {

    private String boardId;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }
}
