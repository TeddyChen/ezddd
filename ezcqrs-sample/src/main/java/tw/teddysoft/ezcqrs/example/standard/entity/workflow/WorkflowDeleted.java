package tw.teddysoft.ezcqrs.example.standard.entity.workflow;

import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public record WorkflowDeleted(String workflowId, UUID id, Instant occurredOn) implements DomainEvent.DestructionEvent {
    @Override
    public String aggregateId() {
        return workflowId;
    }
}
