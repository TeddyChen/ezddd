package tw.teddysoft.ezcqrs.example.standard.usecase;

import org.junit.jupiter.api.Test;
import tw.teddysoft.ezcqrs.example.standard.adapter.*;
import tw.teddysoft.ezcqrs.example.standard.entity.board.Board;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.usecase.board.*;
import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.BoardContentStateProjector;
import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.GetBoardContentInput;
import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.GetBoardContentOutput;
import tw.teddysoft.ezcqrs.example.standard.usecase.boardcontent.GetBoardContentUseCase;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.CreateWorkflowInput;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.CreateWorkflowUseCase;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.WorkflowData;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.WorkflowOutboxMapper;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.core.entity.DomainEvent;
import tw.teddysoft.ezddd.core.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.cqrs.usecase.query.Query;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxRepository;

import java.util.List;
import java.util.UUID;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetBoardContentUseCaseTest {
    @Test
    public void get_board_content() throws Exception {
        DomainEventTypeMapper domainEventTypeMapper = DomainEventTypeMapper.create();
        BoardEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        WorkflowEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(domainEventTypeMapper);
        InMemoryOutboxStore outboxStore = new InMemoryOutboxStore();
        InMemoryQueryStore queryStore = new InMemoryQueryStore();
        RepositoryPeer<BoardData, String> boardPeer = new InMemoryBoardRepositoryPeer(outboxStore);
        RepositoryPeer<WorkflowData, String> workflowPeer = new InMemoryWorkflowRepositoryPeer(outboxStore);
        OutboxMapper<Board, BoardData> boardMapper = new BoardOutboxMapper();
        OutboxMapper<Workflow, WorkflowData> workflowMapper = new WorkflowOutboxMapper();
        Repository<Board, String> boardRepository = new OutboxRepository<Board, BoardData, String>(boardPeer, boardMapper);
        Repository<Workflow, String> workflowRepository = new OutboxRepository<Workflow, WorkflowData, String>(workflowPeer, workflowMapper);
        IsBoardNameExistInquiry inquiry = new IsBoardNameExistInquiry(outboxStore);
        BoardContentStateArchive archive = new BoardContentStateArchive(queryStore);
        BoardContentProjection boardContentProjection = new BoardContentProjection(queryStore);
        BoardContentStateProjector projector = new BoardContentStateProjector(archive);

        Command<CreateBoardInput, CqrsOutput> createBoardUseCase = new CreateBoardUseCase(boardRepository, inquiry);
        String boardId = UUID.randomUUID().toString();
        CreateBoardInput input = new CreateBoardInput();
        input.setBoardId(boardId);
        input.setBoardName("board name");
        createBoardUseCase.execute(input);

        sleep(100);

        String workflow1Id = UUID.randomUUID().toString();
        String workflow2Id = UUID.randomUUID().toString();
        CreateWorkflowInput createWorkflowInput = new CreateWorkflowInput();
        createWorkflowInput.setBoardId(boardId);
        createWorkflowInput.setWorkflowId(workflow1Id);
        createWorkflowInput.setWorkflowName("workflow 1");
        createWorkflowUseCase(workflowRepository).execute(createWorkflowInput);

        sleep(100);

        createWorkflowInput.setWorkflowId(workflow2Id);
        createWorkflowInput.setWorkflowName("workflow 2");
        createWorkflowUseCase(workflowRepository).execute(createWorkflowInput);

        List<DomainEvent> domainEvents = outboxStore.getDomainEvents();
        domainEvents.forEach(event -> projector.project(event));

        Query<GetBoardContentInput, GetBoardContentOutput> getBoardContentUseCase = new GetBoardContentUseCase(boardContentProjection);
        GetBoardContentInput getBoardContentInput = new GetBoardContentInput();
        getBoardContentInput.setBoardId(boardId);
        GetBoardContentOutput getBoardContentOutput = getBoardContentUseCase.execute(getBoardContentInput);
        BoardContentViewModel boardContentViewModel = getBoardContentOutput.getBoardContentViewModel();

        assertEquals(ExitCode.SUCCESS, getBoardContentOutput.getExitCode());
        assertEquals(boardId, boardContentViewModel.getBoardId());
        assertEquals("board name", boardContentViewModel.getBoardName());
        assertEquals(2, boardContentViewModel.getWorkflows().size());
        assertEquals(boardId, boardContentViewModel.getWorkflows().get(0).getBoardId());
        assertEquals(workflow1Id, boardContentViewModel.getWorkflows().get(0).getWorkflowId());
        assertEquals("workflow 1", boardContentViewModel.getWorkflows().get(0).getName());
        assertEquals(boardId, boardContentViewModel.getWorkflows().get(1).getBoardId());
        assertEquals(workflow2Id, boardContentViewModel.getWorkflows().get(1).getWorkflowId());
        assertEquals("workflow 2", boardContentViewModel.getWorkflows().get(1).getName());
    }

    @Test
    public void get_a_deleted_board_content_failed() throws Exception {
        DomainEventTypeMapper domainEventTypeMapper = DomainEventTypeMapper.create();
        BoardEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        WorkflowEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(domainEventTypeMapper);
        InMemoryOutboxStore outboxStore = new InMemoryOutboxStore();
        InMemoryQueryStore queryStore = new InMemoryQueryStore();
        RepositoryPeer<BoardData, String> boardPeer = new InMemoryBoardRepositoryPeer(outboxStore);
        RepositoryPeer<WorkflowData, String> workflowPeer = new InMemoryWorkflowRepositoryPeer(outboxStore);
        OutboxMapper<Board, BoardData> boardMapper = new BoardOutboxMapper();
        OutboxMapper<Workflow, WorkflowData> workflowMapper = new WorkflowOutboxMapper();
        Repository<Board, String> boardRepository = new OutboxRepository<Board, BoardData, String>(boardPeer, boardMapper);
        Repository<Workflow, String> workflowRepository = new OutboxRepository<Workflow, WorkflowData, String>(workflowPeer, workflowMapper);
        IsBoardNameExistInquiry inquiry = new IsBoardNameExistInquiry(outboxStore);
        BoardContentStateArchive archive = new BoardContentStateArchive(queryStore);
        BoardContentProjection boardContentProjection = new BoardContentProjection(queryStore);
        BoardContentStateProjector projector = new BoardContentStateProjector(archive);

        Command<CreateBoardInput, CqrsOutput> createBoardUseCase = new CreateBoardUseCase(boardRepository, inquiry);
        String boardId = UUID.randomUUID().toString();
        CreateBoardInput createBoardInput = new CreateBoardInput();
        createBoardInput.setBoardId(boardId);
        createBoardInput.setBoardName("board name");
        createBoardUseCase.execute(createBoardInput);

        sleep(100);

        String workflow1Id = UUID.randomUUID().toString();
        String workflow2Id = UUID.randomUUID().toString();
        CreateWorkflowInput createWorkflowInput = new CreateWorkflowInput();
        createWorkflowInput.setBoardId(boardId);
        createWorkflowInput.setWorkflowId(workflow1Id);
        createWorkflowInput.setWorkflowName("workflow 1");
        createWorkflowUseCase(workflowRepository).execute(createWorkflowInput);

        sleep(100);

        createWorkflowInput.setWorkflowId(workflow2Id);
        createWorkflowInput.setWorkflowName("workflow 2");
        createWorkflowUseCase(workflowRepository).execute(createWorkflowInput);

        sleep(100);

        Command<DeleteBoardInput, CqrsOutput> deleteBoardUseCase = new DeleteBoardUseCase(boardRepository);
        DeleteBoardInput input = new DeleteBoardInput();
        input.setBoardId(boardId);
        deleteBoardUseCase.execute(input);

        List<DomainEvent> domainEvents = outboxStore.getDomainEvents();
        domainEvents.forEach(event -> projector.project(event));

        Query<GetBoardContentInput, GetBoardContentOutput> getBoardContentUseCase = new GetBoardContentUseCase(boardContentProjection);
        GetBoardContentInput getBoardContentInput = new GetBoardContentInput();
        getBoardContentInput.setBoardId(boardId);
        GetBoardContentOutput getBoardContentOutput = getBoardContentUseCase.execute(getBoardContentInput);

        assertEquals(ExitCode.FAILURE, getBoardContentOutput.getExitCode());
    }

    private CreateWorkflowUseCase createWorkflowUseCase(Repository<Workflow, String> repository) {
       return new CreateWorkflowUseCase(repository);
    }
}
