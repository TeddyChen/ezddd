package tw.teddysoft.ezcqrs.example.standard.usecase;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezcqrs.example.standard.adapter.*;
import tw.teddysoft.ezcqrs.example.standard.entity.board.Board;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardDeleted;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.usecase.board.*;
import tw.teddysoft.ezddd.core.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class DeleteBoardUseCaseTest {
    private Repository<Board, String> repository;
    private IsBoardNameExistInquiry inquiry;
    private FakeListener fakeListener;
    private ExecutorService executor;
    private OutboxStoreListener outboxStoreListener;
    private DomainEventBus domainEventBus;

    @BeforeEach
    public void setUp() {
        DomainEventTypeMapper domainEventTypeMapper = DomainEventTypeMapper.create();
        BoardEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        WorkflowEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(domainEventTypeMapper);
        domainEventBus = new GoogleEventBusAdapter();
        InMemoryOutboxStore outboxStore = new InMemoryOutboxStore();
        outboxStoreListener = new OutboxStoreListener(500, domainEventBus, outboxStore);
        RepositoryPeer<BoardData, String> peer = new InMemoryBoardRepositoryPeer(outboxStore);
        OutboxMapper<Board, BoardData> mapper = new BoardOutboxMapper();
        repository = new OutboxRepository<>(peer, mapper);
        inquiry = new IsBoardNameExistInquiry(outboxStore);
        fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        executor = Executors.newFixedThreadPool(1);
        executor.execute(outboxStoreListener);
    }

    @AfterEach
    public void tearDown() throws Exception {
        outboxStoreListener.shutdown();

        domainEventBus.unregister(fakeListener);
        executor.shutdownNow(); // Cancel currently executing tasks
        // Wait a while for tasks to respond to being cancelled
        try {
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                System.err.println("Executor did not terminate");
            }
        } catch (InterruptedException e) {
        }
    }

    @Test
    public void delete_a_board() {

        Command<CreateBoardInput, CqrsOutput> createBoardUseCase = new CreateBoardUseCase(repository, inquiry);
        CreateBoardInput createBoardInput = new CreateBoardInput();
        createBoardInput.setBoardId(UUID.randomUUID().toString());
        createBoardInput.setBoardName("name");
        CqrsOutput createBoardOutput = createBoardUseCase.execute(createBoardInput);

        Command<DeleteBoardInput, CqrsOutput> deleteBoardUseCase = new DeleteBoardUseCase(repository);
        DeleteBoardInput deleteBoardInput = new DeleteBoardInput();
        deleteBoardInput.setBoardId(createBoardOutput.getId());
        CqrsOutput deleteBoardOutput = deleteBoardUseCase.execute(deleteBoardInput);

        assertEquals(deleteBoardInput.getBoardId(), deleteBoardOutput.getId());
        assertEquals(ExitCode.SUCCESS, deleteBoardOutput.getExitCode());
        assertFalse(repository.findById(deleteBoardOutput.getId()).isPresent());
        await().untilAsserted(() -> assertEquals(1, fakeListener.notifyCount));

    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenBoardDeleted(BoardDeleted event) {
            notifyCount++;
        }
    }

}
