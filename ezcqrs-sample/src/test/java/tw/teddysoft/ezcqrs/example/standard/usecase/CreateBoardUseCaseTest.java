package tw.teddysoft.ezcqrs.example.standard.usecase;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezcqrs.example.standard.adapter.*;
import tw.teddysoft.ezcqrs.example.standard.entity.board.Board;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardCreated;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.usecase.board.BoardData;
import tw.teddysoft.ezcqrs.example.standard.usecase.board.BoardOutboxMapper;
import tw.teddysoft.ezcqrs.example.standard.usecase.board.CreateBoardInput;
import tw.teddysoft.ezcqrs.example.standard.usecase.board.CreateBoardUseCase;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

public class CreateBoardUseCaseTest {

    private DomainEventBus domainEventBus;
    private Repository<Board, String> repository;
    private IsBoardNameExistInquiry inquiry;
    private FakeListener fakeListener;
    private ExecutorService executor;
    private OutboxStoreListener outboxStoreListener;

    @BeforeEach
    public void setUp() {
        DomainEventMapper.setMapper(BoardEventTypeMapper.getInstance());
        domainEventBus = new GoogleEventBusAdapter();
        InMemoryOutboxStore outboxStore = new InMemoryOutboxStore();
        outboxStoreListener = new OutboxStoreListener(500, domainEventBus, outboxStore);
        RepositoryPeer<BoardData, String> peer = new InMemoryBoardRepositoryPeer(outboxStore);
        OutboxMapper<Board, BoardData> mapper = new BoardOutboxMapper();
        repository = new OutboxRepository<>(peer, mapper);
        inquiry = new IsBoardNameExistInquiry(outboxStore);
        fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        executor = Executors.newFixedThreadPool(1);
        executor.execute(outboxStoreListener);
    }

    @AfterEach
    public void tearDown() throws Exception {
        outboxStoreListener.shutdown();

        domainEventBus.unregister(fakeListener);
        executor.shutdownNow(); // Cancel currently executing tasks
        // Wait a while for tasks to respond to being cancelled
        try {
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                System.err.println("Executor did not terminate");
            }
        } catch (InterruptedException e) {
        }
    }

    @Test
    public void create_a_board() {
        Command<CreateBoardInput, CqrsOutput> createBoardUseCase = new CreateBoardUseCase(repository, inquiry);
        CreateBoardInput input = new CreateBoardInput();
        input.setBoardId(UUID.randomUUID().toString());
        input.setBoardName("name");
        CqrsOutput output = createBoardUseCase.execute(input);

        assertEquals(input.getBoardId(), output.getId());
        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertTrue(repository.findById(output.getId()).isPresent());
        Board board = repository.findById(output.getId()).get();
        assertEquals(input.getBoardId(), board.getId());
        assertEquals("name", board.getName());
        await().untilAsserted(() -> assertEquals(1, fakeListener.notifyCount));

    }

    @Test
    public void create_two_boards_with_same_name() {
        CreateBoardInput input1 = new CreateBoardInput();
        String board1Id = UUID.randomUUID().toString();
        input1.setBoardName("name");
        input1.setBoardId(board1Id);
        createBoardUseCase(repository, inquiry).execute(input1);

        CreateBoardInput input2 = new CreateBoardInput();
        String board2Id = UUID.randomUUID().toString();
        input2.setBoardName("name");
        input2.setBoardId(board2Id);
        CqrsOutput board2Output = createBoardUseCase(repository, inquiry).execute(input2);

        assertEquals(ExitCode.FAILURE, board2Output.getExitCode());
        assertEquals("board name " + input2.getBoardName() + " is already used", board2Output.getMessage());
        assertFalse(repository.findById(board2Id).isPresent());

    }

    private Command<CreateBoardInput, CqrsOutput> createBoardUseCase(Repository<Board, String> repository, IsBoardNameExistInquiry inquiry) {
        return new CreateBoardUseCase(repository, inquiry);
    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenBoardCreated(BoardCreated event) {
            notifyCount++;
        }

    }

}
