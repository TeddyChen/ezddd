package tw.teddysoft.ezcqrs.example.standard.usecase;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezcqrs.example.standard.adapter.GoogleEventBusAdapter;
import tw.teddysoft.ezcqrs.example.standard.adapter.InMemoryWorkflowRepository;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.StageCreated;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WipLimit;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.CreateStageInput;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.CreateStageUseCase;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.CreateWorkflowInput;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.CreateWorkflowUseCase;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateStageUseCaseTest {
    @Test
    public void create_a_stage() {
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        Repository<Workflow, String> workflowRepository = new InMemoryWorkflowRepository(domainEventBus);
        Command<CreateWorkflowInput, CqrsOutput> createWorkflowUseCase = new CreateWorkflowUseCase(workflowRepository);
        CreateWorkflowInput createWorkflowInput = new CreateWorkflowInput();
        createWorkflowInput.setBoardId("board id");
        createWorkflowInput.setWorkflowId(UUID.randomUUID().toString());
        createWorkflowInput.setWorkflowName("workflow name");
        CqrsOutput createWorkflowOutput = createWorkflowUseCase.execute(createWorkflowInput);

        Command<CreateStageInput, CqrsOutput> createStageUseCase = new CreateStageUseCase(workflowRepository);
        CreateStageInput input = new CreateStageInput();
        input.setWorkflowId(createWorkflowOutput.getId());
        input.setStageId(UUID.randomUUID().toString());
        input.setStageName("stage name");
        input.setWipLimit(WipLimit.valueOf(3));
        CqrsOutput output = createStageUseCase.execute(input);

        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        Workflow workflow = workflowRepository.findById(input.getWorkflowId()).get();
        assertEquals(1, workflow.getStages().size());
        assertEquals(input.getStageId(), workflow.getStages().get(0).getId());
        assertEquals(input.getStageName(), workflow.getStages().get(0).getStageName());
        assertEquals(input.getWipLimit(), workflow.getStages().get(0).getWipLimit());
        assertEquals(1, fakeListener.notifyCount);
    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenStageCreated(StageCreated event) {
            notifyCount++;
        }

    }
}
