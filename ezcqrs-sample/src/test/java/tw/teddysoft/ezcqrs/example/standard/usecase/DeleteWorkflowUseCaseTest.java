package tw.teddysoft.ezcqrs.example.standard.usecase;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezcqrs.example.standard.adapter.*;
import tw.teddysoft.ezcqrs.example.standard.entity.board.BoardEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowDeleted;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowEventTypeMapper;
import tw.teddysoft.ezcqrs.example.standard.usecase.workflow.*;
import tw.teddysoft.ezddd.cqrs.usecase.command.Command;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.core.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.core.usecase.ExitCode;
import tw.teddysoft.ezddd.core.usecase.Repository;
import tw.teddysoft.ezddd.core.usecase.RepositoryPeer;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventBus;
import tw.teddysoft.ezddd.core.usecase.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxMapper;
import tw.teddysoft.ezddd.core.usecase.outbox.OutboxRepository;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteWorkflowUseCaseTest {
    private Repository<Workflow, String> repository;
    private FakeListener fakeListener;
    private ExecutorService executor;
    private OutboxStoreListener outboxStoreListener;
    private DomainEventBus domainEventBus;

    @BeforeEach
    public void setUp() {
        DomainEventTypeMapper domainEventTypeMapper = DomainEventTypeMapper.create();
        BoardEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        WorkflowEventTypeMapper.getInstance().getMap().forEach((key, value) -> {
            domainEventTypeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(domainEventTypeMapper);
        domainEventBus = new GoogleEventBusAdapter();
        InMemoryOutboxStore outboxStore = new InMemoryOutboxStore();
        outboxStoreListener = new OutboxStoreListener(500, domainEventBus, outboxStore);
        RepositoryPeer<WorkflowData, String> peer = new InMemoryWorkflowRepositoryPeer(outboxStore);
        OutboxMapper<Workflow, WorkflowData> mapper = new WorkflowOutboxMapper();
        repository = new OutboxRepository<Workflow, WorkflowData, String>(peer, mapper);
        fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        executor = Executors.newFixedThreadPool(1);
        executor.execute(outboxStoreListener);
    }

    @AfterEach
    public void tearDown() throws Exception {
        outboxStoreListener.shutdown();

        domainEventBus.unregister(fakeListener);
        executor.shutdownNow(); // Cancel currently executing tasks
        // Wait a while for tasks to respond to being cancelled
        try {
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                System.err.println("Executor did not terminate");
            }
        } catch (InterruptedException e) {
        }
    }

    @Test
    public void delete_a_workflow() {
        DomainEventBus domainEventBus = new GoogleEventBusAdapter();
        Repository<Workflow, String> repository = new InMemoryWorkflowRepository(domainEventBus);
        FakeListener fakeListener = new FakeListener();
        domainEventBus.register(fakeListener);
        Command<CreateWorkflowInput, CqrsOutput> createWorkflowUseCase = new CreateWorkflowUseCase(repository);
        CreateWorkflowInput createWorkflowInput = new CreateWorkflowInput();
        createWorkflowInput.setBoardId("boardId");
        createWorkflowInput.setWorkflowId(UUID.randomUUID().toString());
        createWorkflowInput.setWorkflowName("name");
        CqrsOutput createWorkflowOutput = createWorkflowUseCase.execute(createWorkflowInput);

        Command<DeleteWorkflowInput, CqrsOutput> deleteWorkflowUseCase = new DeleteWorkflowUseCase(repository);
        DeleteWorkflowInput input = new DeleteWorkflowInput();
        input.setWorkflowId(createWorkflowOutput.getId());
        CqrsOutput output = deleteWorkflowUseCase.execute(input);

        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertEquals(input.getWorkflowId(), output.getId());
        assertTrue(repository.findById(output.getId()).isEmpty());
        assertEquals(1, fakeListener.notifyCount);
    }

    @Test
    public void delete_a_workflow_with_outbox_repository() {

        Command<CreateWorkflowInput, CqrsOutput> createWorkflowUseCase = new CreateWorkflowUseCase(repository);
        CreateWorkflowInput createWorkflowInput = new CreateWorkflowInput();
        createWorkflowInput.setBoardId("boardId");
        createWorkflowInput.setWorkflowId(UUID.randomUUID().toString());
        createWorkflowInput.setWorkflowName("name");
        CqrsOutput createWorkflowOutput = createWorkflowUseCase.execute(createWorkflowInput);

        Command<DeleteWorkflowInput, CqrsOutput> deleteWorkflowUseCase = new DeleteWorkflowUseCase(repository);
        DeleteWorkflowInput input = new DeleteWorkflowInput();
        input.setWorkflowId(createWorkflowOutput.getId());
        CqrsOutput output = deleteWorkflowUseCase.execute(input);

        assertEquals(ExitCode.SUCCESS, output.getExitCode());
        assertEquals(input.getWorkflowId(), output.getId());
        assertTrue(repository.findById(output.getId()).isEmpty());
        await().untilAsserted(() -> assertEquals(1, fakeListener.notifyCount));
    }

    class FakeListener {
        int notifyCount = 0;

        @Subscribe
        public void whenWorkflowDeleted(WorkflowDeleted event) {
            notifyCount++;
        }
    }
}
