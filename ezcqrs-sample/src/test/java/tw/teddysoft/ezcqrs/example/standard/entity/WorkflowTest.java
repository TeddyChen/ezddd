package tw.teddysoft.ezcqrs.example.standard.entity;

import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.Workflow;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowCreated;
import tw.teddysoft.ezcqrs.example.standard.entity.workflow.WorkflowDeleted;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class WorkflowTest {

    @Test
    public void create_workflow() {
        Workflow workflow = new Workflow("boardId", "workflowId", "name");

        assertEquals("name", workflow.getName());
        Assert.assertNotNull(workflow.getId());
        assertEquals(1, workflow.getDomainEventSize());
        assertEquals(WorkflowCreated.class, workflow.getLastDomainEvent().getClass());
        Assert.assertFalse(workflow.isDeleted());
    }

    @Test
    public void delete_workflow() {
        Workflow workflow = new Workflow("boardId", "workflowId", "name");
        workflow.markAsDeleted();

        assertTrue(workflow.isDeleted());
        assertEquals(2, workflow.getDomainEventSize());
        assertEquals(WorkflowDeleted.class, workflow.getLastDomainEvent().getClass());
    }
}
