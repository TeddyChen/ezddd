package tw.teddysoft.ezddd.common;

@FunctionalInterface
public interface Converter<From, To> {
    To convert(From value);
}
