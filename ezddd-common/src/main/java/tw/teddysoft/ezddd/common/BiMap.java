package tw.teddysoft.ezddd.common;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code BiMap} is a class extending {@code HashMap} to support get key by
 * value.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class BiMap<K, V> extends HashMap<K, V> {

    private Map<V,K> reverseMap = new HashMap<V,K>();

    @Override
    public V put(K key, V value) {
        reverseMap.put(value, key);
        return super.put(key, value);
    }

    public K getKey(V value){
        return reverseMap.get(value);
    }

}
