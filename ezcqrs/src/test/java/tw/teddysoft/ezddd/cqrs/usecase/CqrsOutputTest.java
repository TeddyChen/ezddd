package tw.teddysoft.ezddd.cqrs.usecase;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CqrsOutputTest {
    @Test
    public void test_CqrsOutput_subclass_can_use_static_create() {
        MyOutput output = MyOutput.create(MyOutput.class);
        assertTrue(output instanceof MyOutput);
    }

    public static class MyOutput extends CqrsOutput<MyOutput> {
        private String myData;

        public String myData() {
            return myData;
        }

        public MyOutput setMyData(String myData) {
            this.myData = myData;
            return this;
        }
    }
}
