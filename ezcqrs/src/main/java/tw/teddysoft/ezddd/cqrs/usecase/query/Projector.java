package tw.teddysoft.ezddd.cqrs.usecase.query;

/**
 * {@code Projector} is a marker interface to represent a service in the use
 * cases layer that writes read model in a query database.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface Projector {
}
