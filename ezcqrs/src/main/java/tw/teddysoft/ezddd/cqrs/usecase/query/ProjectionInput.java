package tw.teddysoft.ezddd.cqrs.usecase.query;

/**
 * {@code ProjectionInput} is a marker interface to represent input data of
 * {@code Projection}.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface ProjectionInput {
}
