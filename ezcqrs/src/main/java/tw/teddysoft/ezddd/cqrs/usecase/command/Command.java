package tw.teddysoft.ezddd.cqrs.usecase.command;


import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Input;
import tw.teddysoft.ezddd.usecase.port.in.interactor.UseCase;

/**
 * {@code Command} is a marker interface for representing a command operation.
 *
 * @param <I> the type parameter for representing a use case input
 * @param <O> the type parameter for representing a use case output
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface Command<I extends Input, O extends CqrsOutput> extends UseCase<I, O> {
}
