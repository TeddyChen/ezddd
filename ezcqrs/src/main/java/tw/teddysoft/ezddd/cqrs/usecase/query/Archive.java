package tw.teddysoft.ezddd.cqrs.usecase.query;

import java.util.Optional;

/**
 * {@code Archive} is the interface for accessing data in the query database.
 *
 * @param <T>   the type parameter for the read model
 * @param <ID>  the type parameter for an aggregate id
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface Archive<T, ID> {

    Optional<T> findById(ID id);

    void save(T data);

    void delete(T data);
}
