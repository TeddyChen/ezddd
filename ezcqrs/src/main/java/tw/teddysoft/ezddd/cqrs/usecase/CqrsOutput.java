package tw.teddysoft.ezddd.cqrs.usecase;

import tw.teddysoft.ezddd.usecase.port.in.interactor.ExitCode;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Output;

/**
 * {@code CqrsOutput} is a class for representing the cqrs output of
 * {@code Command}.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public class CqrsOutput<T extends CqrsOutput<T>> implements Output {
    private String id;
    private String message;
    private ExitCode exitCode;

    /**
     * @since 1.0.7
     */
    public CqrsOutput() {
        message = "";
        exitCode = ExitCode.SUCCESS;
    }

    public static CqrsOutput<?> create(){
        return new CqrsOutput<>();
    }

    /**
     * @since 1.0.5
     */
    public static <T> T create(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getId(){
        return id;
    }

    @Override
    public T setId(String id){
        this.id = id;
        return  self();
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public T setMessage(String message) {
        this.message =  message;
        return self();
    }

    @Override
    public ExitCode getExitCode() {
        return exitCode;
    }

    @Override
    public T setExitCode(ExitCode exitCode) {
        this.exitCode = exitCode;
        return  self();
    }

    /**
     * @since 1.0.7
     */
    @Override
    public T fail() {
        this.exitCode = ExitCode.FAILURE;
        return self();
    }

    /**
     * @since 1.0.7
     */
    @Override
    public T succeed() {
        this.exitCode = ExitCode.SUCCESS;
        return self();
    }

    @SuppressWarnings("unchecked")
    final T self() {
        return (T) this;
    }
}