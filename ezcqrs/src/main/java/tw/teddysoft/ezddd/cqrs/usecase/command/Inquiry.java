package tw.teddysoft.ezddd.cqrs.usecase.command;

/**
 * {@code Inquiry} is an interface to represent a query primarily used by
 * use cases in the command side.
 *
 * @param <I> the type parameter for representing an Inquiry input
 * @param <O> the type parameter for representing an Inquiry output which is
 *           aggregate id or aggregate state
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface Inquiry<I, O> {
    O query(I input);
}
