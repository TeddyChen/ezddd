package tw.teddysoft.ezddd.cqrs.usecase.query;


import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Input;
import tw.teddysoft.ezddd.usecase.port.in.interactor.UseCase;

/**
 * {@code Query} is an interface to represent use case for query.
 *
 * @param <I>  the type parameter for query input
 * @param <O>  the type parameter for query output
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface Query<I extends Input, O extends CqrsOutput> extends UseCase<I, O> {

}
