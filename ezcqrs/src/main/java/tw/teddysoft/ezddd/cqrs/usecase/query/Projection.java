package tw.teddysoft.ezddd.cqrs.usecase.query;

/**
 * {@code Projection} is the interface for building read model that client
 * needed.
 *
 * @param <I>  the type parameter for projection input
 * @param <O>  the type parameter for read model
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface Projection<I extends ProjectionInput, O> {

    O query(I input);
}
