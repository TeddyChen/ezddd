package tw.teddysoft.ezddd.cqrs.usecase.command;

/**
 * {@code InquiryInput} is a marker interface to represent input data of
 * {@code Inquiry}.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0
 */
public interface InquiryInput {
}
