package tw.teddysoft.ezddd.entity;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class DomainEventTypeMapperTest {
    @Test
    public void to_mapping_type_by_class_when_mapping_type_exist() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRegistered", UserRegistered.class);
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertEquals("UserRegistered", defaultMapper.toMappingType(UserRegistered.class));
    }

    @Test
    public void to_mapping_type_by_class_when_mapping_type_not_exist() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertThrows(RuntimeException.class, () -> defaultMapper.toMappingType(UserRegistered.class));
    }

    @Test
    public void to_mapping_type_by_domain_event_when_mapping_type_exist() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRegistered", UserRegistered.class);
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertEquals("UserRegistered", defaultMapper.toMappingType(new UserRegistered(UUID.randomUUID().toString(), UUID.randomUUID(), Instant.now())));
    }

    @Test
    public void to_mapping_type_by_domain_event_when_mapping_type_not_exist() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertThrows(RuntimeException.class, () -> defaultMapper.toMappingType(new UserRegistered(UUID.randomUUID().toString(), UUID.randomUUID(), Instant.now())));
    }

    @Test
    public void contains_mapping_type() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRegistered", UserRegistered.class);
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertTrue(defaultMapper.containsMappingType("UserRegistered"));
    }

    @Test
    public void to_class_when_mapping_type_exist() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRegistered", UserRegistered.class);
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertEquals(UserRegistered.class, defaultMapper.toClass("UserRegistered"));
    }

    @Test
    public void to_class_when_mapping_type_not_exist() {
        var defaultMapper = DomainEventTypeMapper.create();
        defaultMapper.put("UserRenamed", UserRenamed.class);
        assertThrows(RuntimeException.class, () -> defaultMapper.toClass("UserRegistered"));
    }

    public static class UserRegistered implements DomainEvent {
        private String aggregateId;
        private UUID eventId;
        private Instant occurredOn;

        public UserRegistered(String aggregateId, UUID eventId, Instant occurredOn) {
            this.aggregateId = aggregateId;
            this.eventId = eventId;
            this.occurredOn = occurredOn;
        }

        @Override
        public UUID id() {
            return eventId;
        }

        @Override
        public Instant occurredOn() {
            return occurredOn;
        }

        @Override
        public String aggregateId() {
            return aggregateId;
        }
    }

    public static class UserRenamed implements DomainEvent {
        private String aggregateId;
        private String name;
        private UUID eventId;
        private Instant occurredOn;

        public UserRenamed(String aggregateId, String name, UUID eventId, Instant occurredOn) {
            this.aggregateId = aggregateId;
            this.name = name;
            this.eventId = eventId;
            this.occurredOn = occurredOn;
        }

        @Override
        public UUID id() {
            return eventId;
        }

        @Override
        public Instant occurredOn() {
            return occurredOn;
        }

        @Override
        public String aggregateId() {
            return aggregateId;
        }
    }
}
