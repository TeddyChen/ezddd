package tw.teddysoft.ezddd.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.entity.EsAggregateRoot;
import tw.teddysoft.ucontract.ClassInvariantViolationException;
import tw.teddysoft.ucontract.Contract;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EsAggregateRootTest {
    @Test
    public void apply_a_construction_domain_event() {
        var user = new User();
        assertEquals(1, user.getDomainEventSize());
        assertEquals(user.getId(), user.getLastDomainEvent().aggregateId());
        assertEquals(1, user.whenCount);
        assertEquals(1, user.invariantCount);
    }

    @Test
    public void apply_a_domain_event() {
        var user = new User();
        user.rename("new name");
        assertEquals("new name", user.name);
        assertEquals(2, user.getDomainEventSize());
        assertEquals(user.getId(), user.getLastDomainEvent().aggregateId());
        assertEquals(2, user.whenCount);
        assertEquals(3, user.invariantCount);
    }

    @Test
    public void apply_a_domain_event_lead_to_violate_class_invariant() {
        var user = new User();
        assertThrows(ClassInvariantViolationException.class, () -> user.rename(null));
    }

    @Test
    public void apply_a_domain_event_causing_runtime_exception() {
        var user = new User();
        assertThrows(RuntimeException.class, () -> user.renameWithRuntimeException("name"));
    }

    @Test
    public void apply_a_domain_event_causing_runtime_exception_and_violate_class_invariant() {
        var user = new User();
        var ce = assertThrows(ClassInvariantViolationException.class, () -> user.renameWithRuntimeException(null));
        assertEquals(RuntimeException.class, ce.getCause().getClass());
        assertEquals("User exception threw.", ce.getCause().getMessage());
    }

    @Test
    public void replay_events() {
        List<DomainEvent> domainEventList = new ArrayList<>();
        var userId = UUID.randomUUID().toString();
        domainEventList.add(new UserRegistered("name", userId, UUID.randomUUID(), Instant.now()));
        domainEventList.add(new UserRenamed("new name", userId, UUID.randomUUID(), Instant.now()));
        var user = new User(domainEventList);
        assertEquals(userId, user.getId());
        assertEquals("new name", user.name);
        assertEquals(0, user.getDomainEventSize());
    }

    public static class User extends EsAggregateRoot {
        private String id;
        private String name;
        public int whenCount = 0;
        public int invariantCount = 0;

        public User() {
            apply(new UserRegistered("name", UUID.randomUUID().toString(), UUID.randomUUID(), Instant.now()));
        }

        public User(List<? extends DomainEvent> events) {
            super(events);
        }

        @Override
        protected void ensureInvariant() {
            Contract.invariant("name can not be null.", () -> null != name);
            invariantCount++;
        }

        @Override
        protected void when(DomainEvent event) {
            whenCount++;
            if (event instanceof UserRegistered e) {
                id = e.aggregateId();
                name = e.name();
            }
            else if (event instanceof UserRenamed e) {
                name = e.name();
            }
            else if (event instanceof UserExceptionThrew e) {
                name = e.name();
                throw new RuntimeException("User exception threw.");
            }
        }

        @Override
        public String getCategory() {
            return null;
        }

        @Override
        public boolean isDeleted() {
            return false;
        }

        @Override
        public String getId() {
            return id;
        }

        public void rename(String newName) {
            apply(new UserRenamed(newName, id, UUID.randomUUID(), Instant.now()));
        }

        public void renameWithRuntimeException(String newName) {
            apply(new UserExceptionThrew(newName, id, UUID.randomUUID(), Instant.now()));
        }
    }

    public static class UserRegistered implements DomainEvent, DomainEvent.ConstructionEvent {
        private String name;
        private String aggregateId;
        private UUID eventId;
        private Instant occurredOn;

        public UserRegistered(String name, String aggregateId, UUID eventId, Instant occurredOn){
            this.name = name;
            this.aggregateId = aggregateId;
            this.eventId = eventId;
            this.occurredOn = occurredOn;
        }


        @Override
        public UUID id() {
            return eventId;
        }

        @Override
        public Instant occurredOn() {
            return occurredOn;
        }

        @Override
        public String aggregateId() {
            return aggregateId;
        }

        public String name() {
            return name;
        }
    }

    public static class UserRenamed implements DomainEvent {
        private String name;
        private String aggregateId;
        private UUID eventId;
        private Instant occurredOn;

        public UserRenamed(String name, String aggregateId, UUID eventId, Instant occurredOn){
            this.name = name;
            this.aggregateId = aggregateId;
            this.eventId = eventId;
            this.occurredOn = occurredOn;
        }

        @Override
        public UUID id() {
            return eventId;
        }

        @Override
        public Instant occurredOn() {
            return occurredOn;
        }

        @Override
        public String aggregateId() {
            return aggregateId;
        }

        public String name() {
            return name;
        }
    }

    public static class UserExceptionThrew implements DomainEvent {
        private String name;
        private String aggregateId;
        private UUID eventId;
        private Instant occurredOn;

        public UserExceptionThrew(String name, String aggregateId, UUID eventId, Instant occurredOn){
            this.name = name;
            this.aggregateId = aggregateId;
            this.eventId = eventId;
            this.occurredOn = occurredOn;
        }

        @Override
        public UUID id() {
            return eventId;
        }

        @Override
        public Instant occurredOn() {
            return occurredOn;
        }

        @Override
        public String aggregateId() {
            return aggregateId;
        }

        public String name() {
            return name;
        }
    }
}
