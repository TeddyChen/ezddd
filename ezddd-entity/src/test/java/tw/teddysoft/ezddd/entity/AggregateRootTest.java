package tw.teddysoft.ezddd.entity;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AggregateRootTest {
    @Test
    public void apply_a_domain_event() {
        var user = new User();
        assertEquals(1, user.getDomainEventSize());
        assertEquals(user.getLastDomainEvent().aggregateId(), user.getId());
        assertEquals("UnmodifiableList", user.getDomainEvents().getClass().getSuperclass().getSimpleName());
    }

    public static class User extends AggregateRoot {
        private String id;

        public User(){
            id = UUID.randomUUID().toString();
            apply(new UserRegistered(id, UUID.randomUUID(), Instant.now()));

        }

        @Override
        public boolean isDeleted() {
            return false;
        }

        @Override
        public String getId() {
            return id;
        }
    }

    public static class UserRegistered implements DomainEvent, DomainEvent.ConstructionEvent {
        private String aggregateId;
        private UUID eventId;
        private Instant occurredOn;

        public UserRegistered(String aggregateId, UUID eventId, Instant occurredOn){
            this.aggregateId = aggregateId;
            this.eventId = eventId;
            this.occurredOn = occurredOn;
        }


        @Override
        public UUID id() {
            return eventId;
        }

        @Override
        public Instant occurredOn() {
            return occurredOn;
        }

        @Override
        public String aggregateId() {
            return aggregateId;
        }
    }

}


