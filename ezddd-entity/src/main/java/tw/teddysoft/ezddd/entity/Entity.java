package tw.teddysoft.ezddd.entity;

import java.io.Serializable;

/**
 * {@code Entity} is an interface for representing domain-driven design entity.
 *
 * @param <ID> the type parameter for entity id
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface Entity<ID> extends Serializable {
    ID getId();
}

