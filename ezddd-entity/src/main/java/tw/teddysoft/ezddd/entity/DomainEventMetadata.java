//package tw.teddysoft.ezddd.entity;
//
//import tw.teddysoft.ezddd.entity.common.Json;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * {@code DomainEventMetadata} is a class to represent a capability of
// * applying and storing domain events.
// *
// * @author Teddy Chen
// * @author ezKanban team
// * @since 1.0
// */
//public class DomainEventMetadata {
//    private final Map<String, Object> metadata;
//
//    public DomainEventMetadata() {
//        this.metadata = new HashMap<>();
//    }
//
//    public void append(String key, Object value) {
//        metadata.put(key, value);
//    }
//
//    public String asJsonString() {
//        return Json.asString(metadata);
//    }
//
//}
