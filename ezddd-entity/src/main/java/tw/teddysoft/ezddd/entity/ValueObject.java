package tw.teddysoft.ezddd.entity;

import java.io.Serializable;

/**
 * {@code ValueObject} is an interface for representing domain-driven design
 * value object.
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface ValueObject extends Serializable {
}
