package tw.teddysoft.ezddd.entity;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
/**
 * {@code DomainEvent} is an interface for representing domain events in 
 * domain-driven design.
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface DomainEvent extends Serializable {
	UUID id();

	Instant occurredOn();

	String aggregateId();

	/**
	 * The domain event which is created when initializing object.
	 */
	interface ConstructionEvent extends DomainEvent {}

	/**
	 * The domain event which is created when deleting object.
	 */
	interface DestructionEvent extends DomainEvent {}
}

