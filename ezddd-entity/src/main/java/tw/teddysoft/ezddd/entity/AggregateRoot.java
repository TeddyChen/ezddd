package tw.teddysoft.ezddd.entity;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * {@code AggregateRoot} is an abstract class for representing an aggregate root
 * in domain-driven design. All concrete aggregate roots must be descendants of 
 * this class. 
 *
 * @param <ID> the type parameter for aggregate root id
 * @param <E>  the type parameter for domain event
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public abstract class AggregateRoot<ID, E extends DomainEvent> implements Entity<ID>, DomainEventSource<E> {

    private List<E> domainEvents;
    private long version;
    protected boolean isDeleted;

//region: Constructors
    protected AggregateRoot() {
        super();
        domainEvents = new CopyOnWriteArrayList<>();
        version = -1;
        isDeleted = false;
    }
    //endregion

//region: Behaviors for the aggregate root
    public boolean isDeleted() {
        return isDeleted;
    };

    protected void addDomainEvent(E event) {
        domainEvents.add(event);
        version++;
    }
    //endregion

    //region: Behaviors for the DomainEventSource
    @Override
    public void apply(E event) {
        addDomainEvent(event);
    }

    @Override
    public void clearDomainEvents() {
        domainEvents.clear();
    }

    @Override
    public List<E> getDomainEvents() {
        return Collections.unmodifiableList(domainEvents);
    }

    @Override
    public E getLastDomainEvent() {
        return domainEvents.get(domainEvents.size()-1);
    }

    @Override
    public int getDomainEventSize(){
        return domainEvents.size();
    }
    //endregion

    //region: Optimistic lock
    public void setVersion(long version) {
        this.version = version;
    }

    public long getVersion() {
        return version;
    }
    //endregion
}
