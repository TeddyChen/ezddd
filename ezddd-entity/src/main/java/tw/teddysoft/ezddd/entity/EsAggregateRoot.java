package tw.teddysoft.ezddd.entity;

import tw.teddysoft.ucontract.ClassInvariantViolationException;

import java.util.List;

import static tw.teddysoft.ucontract.Contract.ensure;
import static tw.teddysoft.ucontract.Contract.requireNotNull;

/**
 * {@code EsAggregateRoot} is a class for representing domain-driven design
 * aggregate root using event sourcing.
 *
 * @param <ID> the type parameter for aggregate root id
 * @param <E>  the type parameter for domain event
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public abstract class EsAggregateRoot<ID, E extends DomainEvent> extends AggregateRoot<ID, E> {

//region :Constructors
    protected EsAggregateRoot() {
        super();
    }

    public EsAggregateRoot(List<E> events) {
        this();
        requireNotNull("Domain events", events);

        replayEvents(events);
        clearDomainEvents();

        ensure("Domain event size is 0", () -> getDomainEvents().size() == 0);
        ensure("Aggregate id cannot be null", () -> null != getId());
    }

    protected void replayEvents(List<E> events) {
        events.forEach(this::apply);
    }
    //endregion

    //region :Behaviors for the DomainEventSource
    // template method
    @Override
    public final void apply(E event) {
        RuntimeException leadingException = null;
        if (!(event instanceof DomainEvent.ConstructionEvent))
            ensureInvariant();
        try {
            when(event);
        } catch (RuntimeException e) {
            leadingException = e;
        } finally {
            if (!(event instanceof DomainEvent.DestructionEvent))
                ensureInvariant(leadingException);
        }

        addDomainEvent(event);
    }
    //endregion

    private void ensureInvariant(RuntimeException leadingException) {
        try {
            ensureInvariant();
            if (null != leadingException)
                throw leadingException;
        } catch (ClassInvariantViolationException ce) {
            ce.initCause(leadingException);
            throw ce;
        }
    }

    //region hook methods
    protected void ensureInvariant() {
    }

    protected abstract void when(E event);
    //endregion

    //region: event sourcing
    abstract public String getCategory();

    public final String getStreamName() {
        return getCategory() + "-" + getId().toString();
    }

    public static String getStreamName(String category, String id) {
        return category + "-" + id;
    }
    //endregion
}
