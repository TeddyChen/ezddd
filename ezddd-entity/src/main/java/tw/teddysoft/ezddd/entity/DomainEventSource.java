package tw.teddysoft.ezddd.entity;

import java.util.List;

/**
 * {@code DomainEventSource} is an interface to represent a capability of
 * applying and storing domain events.
 *
 * @param <E>  the type parameter for domain event
 *
 * @author Teddy Chen 
 * @author ezKanban team
 * @since 1.0
 */
public interface DomainEventSource<E extends DomainEvent> {
    void apply(E event);
    void clearDomainEvents();

    List<E> getDomainEvents();
    E getLastDomainEvent();
    int getDomainEventSize();
}
